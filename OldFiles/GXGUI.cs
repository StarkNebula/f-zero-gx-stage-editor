﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class GXGUI
{
    private const uint guiSpace = 15;

    private enum EffectType
    {
        FinishLine = 0,
        Heal = 1,
        UNKNOWN_2 = 2,
        Boost = 3,
        Jump = 4,
    }

    public static void AIVisibleField(ref bool[] enableField, AiVisibleEffects editorTarget)
    {
        if (enableField[0])
        {
            EditorGUILayout.FloatField("Spline Position From", editorTarget.splinePositionFrom);
            EditorGUILayout.FloatField("Spline Position To", editorTarget.splinePositionTo);
            EditorGUILayout.FloatField("Difference", editorTarget.splinePositionTo - editorTarget.splinePositionFrom);
        }

        if (enableField[1])
        {
            EditorGUILayout.FloatField("Position Width Left", editorTarget.widthLeft);
            EditorGUILayout.FloatField("Position Width Right", editorTarget.widthRight);
            ///EditorGUILayout.FloatField("Difference", editorTarget.splinePositionTo - editorTarget.splinePositionFrom);
        }

        if (enableField[2])
        {
            CustomGUI.SetGUIColor(editorTarget.trackBranchID != 0, Palette.grey, Palette.white);
            EditorGUILayout.IntField("Branch ID:   " + editorTarget.trackBranchID, editorTarget.trackBranchID);
            CustomGUI.ResetGUIColor();
            CustomGUI.SetGUIColorWhitten(editorTarget.effectType, Palette.grey, Palette.rose, Palette.cyan, Palette.yellow, Palette.orange);
            EditorGUILayout.IntField("Effect Type: " + ((EffectType)editorTarget.effectType).ToString(), editorTarget.effectType);
            CustomGUI.ResetGUIColor();
        }

        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }
    public static void AnimationType1Field(ref bool[] enableField, AnimationType1 editorTarget)
    {
        if (enableField[0])
        {
            EditorGUILayout.Vector3Field("Position?", editorTarget.position);
        }

        if (enableField[1])
        {
            EditorGUILayout.Vector3Field("Rotation?", editorTarget.rotation);
        }

        if (enableField[2])
        {
            CustomGUI.SetGUIColor(editorTarget.id_1 != "0000 0000", Palette.red.Whitten(.5f), Palette.white);
            EditorGUILayout.TextField("ID_1", editorTarget.id_1);
            CustomGUI.ResetGUIColor();
            CustomGUI.SetGUIColor(editorTarget.id_2 != "0000 0000", Palette.cyan.Whitten(.5f), Palette.white);
            EditorGUILayout.TextField("ID_2", editorTarget.id_2);
            CustomGUI.ResetGUIColor();
            CustomGUI.SetGUIColor(editorTarget.potId != 0, Palette.orange.Whitten(.5f), Palette.white);
            EditorGUILayout.IntField("POT_ID", (int)editorTarget.potId);
            CustomGUI.ResetGUIColor();
        }

        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }
    public static void AnimationType2Field(ref bool[] enableField, AnimationType2 editorTarget)
    {
        if (enableField[0])
        {
            EditorGUILayout.Vector3Field("Position?", editorTarget.position);
        }

        if (enableField[1])
        {
            EditorGUILayout.Vector3Field("Rotation?", editorTarget.rotation);
        }

        if (enableField[2])
        {
            CustomGUI.SetGUIColor(editorTarget.unk_1 != 0, Palette.red.Whitten(.5f), Palette.white);
            EditorGUILayout.IntField("Unk_1", (int)editorTarget.unk_1);
            CustomGUI.ResetGUIColor();
            CustomGUI.SetGUIColor(editorTarget.unk_2 != 0, Palette.cyan.Whitten(.5f), Palette.white);
            EditorGUILayout.IntField("Unk_2", (int)editorTarget.unk_1);
            CustomGUI.ResetGUIColor();
            CustomGUI.SetGUIColor(editorTarget.id != 0, Palette.orange.Whitten(.5f), Palette.white);
            EditorGUILayout.IntField("POT_ID", (int)editorTarget.id);
            CustomGUI.ResetGUIColor();
        }

        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }
    public static void EffectPathField(ref bool[] enableField, EffectPath editorTarget)
    {
        if (enableField[0])
        {
            EditorGUILayout.Vector3Field("Start Positions", editorTarget.pathPositionStart);
        }

        if (enableField[1])
        {
            EditorGUILayout.Vector3Field("End Positions", editorTarget.pathPositionEnd);
        }

        if (enableField[2])
        {
            EditorGUILayout.TextField("? 0x0C", editorTarget.unk_0x0C);
            //EditorGUILayout.TextField("? 0x10", editorTarget.null_0x10);
            EditorGUILayout.IntField("ID", (int)editorTarget.id);
        }

        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }
    public static void AXCheckpointField(ref bool[] enableField, AxCheckpoint editorTarget)
    {
        if (enableField[0])
        {
            EditorGUILayout.Vector3Field("Global Position", editorTarget.globalPosition);
        }

        if (enableField[1])
        {
            EditorGUILayout.Vector3Field("Local Scale", editorTarget.localScale);
        }

        if (enableField[2])
        {
            EditorGUILayout.IntField("Track Branch ID", (int)editorTarget.trackBranchID);
            EditorGUILayout.TextField("UNK 0x0C", editorTarget.unk_0x0C);
            EditorGUILayout.TextField("UNK 0x10", editorTarget.unk_0x10);
        }

        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }
    public static void StoryModeDataField(ref bool[] enableField, StoryModeSpecialData editorTarget)
    {
        if (enableField[0])
            EditorGUILayout.Vector3Field("Collider Scale", editorTarget.triggerScale);

        if (enableField[1])
            EditorGUILayout.Vector3Field("Position", editorTarget.position);

        if (enableField[2])
            EditorGUILayout.Vector3Field("Rotation", editorTarget.rotation);

        if (enableField[3])
            EditorGUILayout.Vector3Field("Scale", editorTarget.scale);

        if (enableField[4])
        {
            EditorGUILayout.TextField("Level Data [0000][]2[][LV]", editorTarget.leveldata);
            CustomGUI.SetGUIColor(editorTarget.unk_0x10 != 0, Palette.cyan.Whitten(.5f), Palette.white);
            EditorGUILayout.IntField("UNK_Offset?", (int)editorTarget.unk_0x10);
            CustomGUI.ResetGUIColor();
        }


        if (CustomGUI.IsAnyTrue(enableField))
            GUILayout.Space(guiSpace);
    }

    #region AI Effects Editor
    [CustomEditor(typeof(DisplayAIEffects))]
    public class DisplayAIEffects_Editor : Editor
    {

        private static bool[] enableField = new bool[3];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayAIEffects editorTarget = target as DisplayAIEffects;

            CustomGUI.Button(ref enableField[0], "Position Relative to Spline");
            CustomGUI.Button(ref enableField[1], "Position and Width of effect (%)");
            CustomGUI.Button(ref enableField[2], "IDs");

            for (int i = 0; i < editorTarget.aiVisibleEffects.Length; i++)
                GXGUI.AIVisibleField(ref enableField, editorTarget.aiVisibleEffects[i]);
        }
    }
    #endregion
    #region Animation 1 Editor
    [CustomEditor(typeof(DisplayAnimationType1))]
    public class DisplayAnimationType1_Editor : Editor
    {
        private static bool[] enableField = new bool[3];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayAnimationType1 editorTarget = target as DisplayAnimationType1;

            CustomGUI.Button(ref enableField[0], "Position");
            CustomGUI.Button(ref enableField[1], "Rotations");
            CustomGUI.Button(ref enableField[2], "IDs");

            for (int i = 0; i < editorTarget.animationType1.Length; i++)
                GXGUI.AnimationType1Field(ref enableField, editorTarget.animationType1[i]);
        }
    }
    #endregion
    #region Animation 2 Editor
    [CustomEditor(typeof(DisplayAnimationType2))]
    public class DisplayAnimationType2_Editor : Editor
    {
        private static bool[] enableField = new bool[3];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayAnimationType2 editorTarget = target as DisplayAnimationType2;

            CustomGUI.Button(ref enableField[0], "Position");
            CustomGUI.Button(ref enableField[1], "Rotations");
            CustomGUI.Button(ref enableField[2], "IDs");

            for (int i = 0; i < editorTarget.animationType2.Length; i++)
                GXGUI.AnimationType2Field(ref enableField, editorTarget.animationType2[i]);
        }
    }
    #endregion
    #region Effect Path Editor
    [CustomEditor(typeof(DisplayPathEffect))]
    public class DisplayPathEffect_Editor : Editor
    {
        private static bool[] enableField = new bool[3];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayPathEffect editorTarget = target as DisplayPathEffect;

            CustomGUI.Button(ref enableField[0], "Start Position");
            CustomGUI.Button(ref enableField[1], "End Position");
            CustomGUI.Button(ref enableField[2], "IDs");

            for (int i = 0; i < editorTarget.effectPath.Length; i++)
                GXGUI.EffectPathField(ref enableField, editorTarget.effectPath[i]);
        }
    }
    #endregion
    #region AX Checkpoint
    [CustomEditor(typeof(DisplayAxCheckPoint))]
    public class DisplayAxCheckPoint_Editor : Editor
    {
        private static bool[] enableField = new bool[3];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayAxCheckPoint editorTarget = target as DisplayAxCheckPoint;

            CustomGUI.Button(ref enableField[0], "Global Positions");
            CustomGUI.Button(ref enableField[1], "Local Scales");
            CustomGUI.Button(ref enableField[2], "Track Branch ID");

            for (int i = 0; i < editorTarget.axCheckpoint.Length; i++)
                GXGUI.AXCheckpointField(ref enableField, editorTarget.axCheckpoint[i]);
        }
    }
    #endregion
    #region Special Story Data
    [CustomEditor(typeof(DisplayStoryModeSpecialData))]
    public class DisplayStoryModeSpecialData_Editor : Editor
    {
        private static bool[] enableField = new bool[5];

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DisplayStoryModeSpecialData editorTarget = target as DisplayStoryModeSpecialData;

            CustomGUI.Button(ref enableField[0], "Collider Scale");
            CustomGUI.Button(ref enableField[1], "Positions");
            CustomGUI.Button(ref enableField[2], "Rotations");
            CustomGUI.Button(ref enableField[3], "Scales");
            CustomGUI.Button(ref enableField[4], "IDs + Offset?");

            for (int i = 0; i < editorTarget.storyModeData.Length; i++)
                GXGUI.StoryModeDataField(ref enableField, editorTarget.storyModeData[i]);
        }
    }
    #endregion
}