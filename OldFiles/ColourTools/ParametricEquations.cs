﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ParametricEquations : MonoBehaviour {

    // Resources
    //https://en.wikipedia.org/wiki/Waveform
    //https://en.wikipedia.org/wiki/Parametric_equation
    //https://en.wikipedia.org/wiki/Hypotrochoid
    //https://en.wikipedia.org/wiki/Asymptote
    //https://en.wikipedia.org/wiki/Butterfly_curve_(transcendental)

    //https://chicounity3d.wordpress.com/2014/05/23/how-to-lerp-like-a-pro/
    //http://wiki.unity3d.com/index.php?title=Interpolate

    //https://johncarlosbaez.wordpress.com/2013/12/03/rolling-hypocycloids/
    //http://math.ucr.edu/home/baez/rolling/rolling_1.html
    //https://www.pinterest.com/peneloped63/my-math-art/


    public enum Equation
    {
        ButterflyCurve,
    }
    public Equation equation = Equation.ButterflyCurve;

    // Does a thing

}
