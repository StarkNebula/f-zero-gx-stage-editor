using UnityEngine;

public static class Palette
{
    #region Colour Modules (Structs)
    // Add Shade structs here
    public static MonochromePalette Monochrome = new MonochromePalette();

    // Add Colour structs here
    public static NeonPalette Neon = new NeonPalette();
	public static DarkPalette Dark = new DarkPalette();
	public static PastelPalette Pastel = new PastelPalette();
    #endregion
    #region Generic Colours & Parameters
    // Full saturation.
    private const float offsetR = 2f;
    private const float offsetG = 0f;
    private const float offsetB = 4f;
    private const int depth = 24;
    private const float min = 0.0f;
    private const float max = 1.0f;

    //
    public static Color red            { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 0, depth, min, max);  } }
    public static Color red_orange     { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 1, depth, min, max);  } }
    public static Color orange         { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 2, depth, min, max);  } }
    public static Color yellow_orange  { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 3, depth, min, max);  } }
    public static Color yellow         { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 4, depth, min, max);  } }
    public static Color yellow_lime    { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 5, depth, min, max);  } }
    public static Color lime           { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 6, depth, min, max);  } }
    public static Color lime_green     { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 7, depth, min, max);  } }
    public static Color green          { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 8, depth, min, max);  } }
    public static Color green_teal     { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 9, depth, min, max);  } }
    public static Color teal           { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 10, depth, min, max); } }
    public static Color cyan_teal      { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 11, depth, min, max); } }
    public static Color cyan           { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 12, depth, min, max); } }
    public static Color cyan_cobalt    { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 13, depth, min, max); } }
    public static Color cobalt         { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 14, depth, min, max); } }
    public static Color cobalt_blue    { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 15, depth, min, max); } }
    public static Color blue           { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 16, depth, min, max); } }
    public static Color blue_violet    { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 17, depth, min, max); } }
    public static Color violet         { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 18, depth, min, max); } }
    public static Color magenta_violet { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 19, depth, min, max); } }
    public static Color magenta        { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 20, depth, min, max); } }
    public static Color magenta_rose   { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 21, depth, min, max); } }
    public static Color rose           { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 22, depth, min, max); } }
    public static Color rose_red       { get { return BaseColourAlgorith(offsetR, offsetG, offsetB, 23, depth, min, max); } }
	//
    public static Color black { get { return BaseShadeAlgorithm(0, 2, min, max); } }
    public static Color grey  { get { return BaseShadeAlgorithm(1, 2, min, max); } }
    public static Color white { get { return BaseShadeAlgorithm(2, 2, min, max); } }
    #endregion

    #region Base Methods for this class
    // TODO
    // THIS SECTION COULD USE SOME REWRITING
    /// <summary>
    /// Given the offset of the R/G/B channel, returns the R/G/B channel float value of the colour.
    /// stepJ ofK means where along a cycle you wish to get. ei: 1 / 6 (16.67%) would return 1f.
    /// If you were to look at a colour cycling through RYGCBM, you would notice this pattern:
    /// A colour channel's cycle goes: (0-1) -> (1) -> (1) -> (1-0) -> (0) -> (0) -> (restart).
    /// This means you can generate RYGCBM colours by offseting the RGB channels a bit.
    /// offsetRGBCMY is the offset. If you made Red, you could create a new Color and pass in
    /// an offset of 2f for R, 0f for G, and 4f for B, and you would get (1f, 0f, 0f).
    /// min/max are the min/max values this method will output. If we take the previous example,
    /// we could threshold the values to 0.2f and 0.8f, thus (0.8f, 0.2f, 0.2f).
    /// </summary>
    /// <param name="offsetRGBCMY"></param>
    /// <param name="stepJ"></param>
    /// <param name="ofK"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static float ColourLerp(float offsetRGBCMY, float stepJ, float ofK, float min, float max)
    {
        // Convert (j/k) to percent. ei: we are 63% through a full colour cycle.
        // Convert (j/k) percentage into a value that ranges from 0-6.
        // Add the offset. It may "overflow" over 6, but we will modulo that later.
        float pointAlongColourCycle = (((stepJ / ofK) % 1f) * 6f) + offsetRGBCMY;

        // Cast to int, results in value from 0-5
        int pointAlongColourCycleAsInteger = (int)pointAlongColourCycle % 6;

        // Evaluate point in switch case
        switch (pointAlongColourCycleAsInteger)
        {
            case 0:
                // Modulo for safety
                return Mathf.Lerp(min, max, pointAlongColourCycle % 1f);

            case 1:
            case 2:
                return max;

            case 3:
                // Modulo for safety
                return Mathf.Lerp(max, min, pointAlongColourCycle % 1f);

            case 4:
            case 5:
                return min;

            default:
                Debug.LogError("Palette.ColourLerp() did not receive an integer value from 0-5 as expected.");
                break;
        }
        return 0f;
    }

    /// <summary>
    /// Used to generate colours
    /// </summary>
    /// <param name="offsetR"></param>
    /// <param name="offsetG"></param>
    /// <param name="offsetB"></param>
    /// <param name="selectDepth"></param>
    /// <param name="depth"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static Color BaseColourAlgorith(float offsetR, float offsetG, float offsetB, int selectDepth, int depth, float min, float max)
    {
        return new Color(
            ColourLerp(offsetR, selectDepth, depth, min, max),
            ColourLerp(offsetG, selectDepth, depth, min, max),
            ColourLerp(offsetB, selectDepth, depth, min, max),
            1);
    }
    /// <summary>
    /// Used to generate monochrome shades
    /// </summary>
    /// <param name="selectDepth"></param>
    /// <param name="depth"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static Color BaseShadeAlgorithm(float selectDepth, float depth, float min, float max)
    {
        return new Color(
            Mathf.Lerp(min, max, selectDepth / depth),
            Mathf.Lerp(min, max, selectDepth / depth),
            Mathf.Lerp(min, max, selectDepth / depth)
        );
    }
    #endregion

    #region Utility Methods
    /// <summary>
    /// Returns a color from the palette type. This method is useful in for loops.
    /// Specifically selected of depth.
    /// Ex: 2 / 24 = orange.
    /// 3 / 24 = yellow orange.
    /// 4 / 24 = yellow.
    /// /// </summary>
    /// <param name="thisColor"></param>
    /// <param name="ofThisDepth"></param>
    /// <returns></returns>
    public static Color ColorWheel(int thisColor, int ofThisDepth)
    {
        // These values come from the above set which describe Generic Colours
        return BaseColourAlgorith(offsetR, offsetG, offsetB, thisColor, ofThisDepth, min, max);
    }

    #region Mix Methods
    /// <summary>
    /// Base function for mixing colours.
    /// </summary>
    /// <param name="partsA"></param>
    /// <param name="colorA"></param>
    /// <param name="partsB"></param>
    /// <param name="colorB"></param>
    /// <returns></returns>
    private static Color Mix2Colours(float partsA, Color colorA, float partsB, Color colorB)
    {
		// Average
		float totalParts = partsA + partsB;
		
		Color newColor = new Color(){
			r = ((colorA.r * partsA) + (colorB.r * partsB))/totalParts,
			g = ((colorA.g * partsA) + (colorB.g * partsB))/totalParts,
			b = ((colorA.b * partsA) + (colorB.b * partsB))/totalParts,
			a = ((colorA.a * partsA) + (colorB.a * partsB))/totalParts
		};

		return newColor;
	}
    /// <summary>
    /// Returns a mix of (n) parts ColorA with (n) parts ColorB.
    /// </summary>
    /// <param name="partsA">Ratio of colorA.</param>
    /// <param name="colorA">Color a.</param>
    /// <param name="partsB">Ratio of colorB.</param>
    /// <param name="colorB">Color b.</param>
    public static Color Mix(Color colorA, float partsA, Color colorB, float partsB)
    {
        Color newColor = Mix2Colours (partsA, colorA, partsB, colorB);
		return newColor;
	}
    /// <summary>
    /// Returns a mix of (n) parts ColorA with 1 part ColorB.
    /// </summary>
    /// <param name="partsA">Number of parts colorA is used</param>
    /// <param name="colorA">Color a.</param>
    /// <param name="colorB">Color b.</param>
    public static Color Mix(Color colorA, float partsA, Color colorB)
    {
        Color newColor = Mix2Colours (partsA, colorA, 1, colorB);
		return newColor;
	}
    /// <summary>
    /// Returns a mix of 1 part ColorA with (n) parts ColorB.
    /// </summary>
    /// <param name="colorA">Color a.</param>
    /// <param name="partsB">Ratio of colorB</param>
    /// <param name="colorB">Color b.</param>
    public static Color Mix(Color colorA, Color colorB, float partsB)
    {
        Color newColor = Mix2Colours (1, colorA, partsB, colorB);
		return newColor;
	}
	/// <summary>
	/// Returns a mix of ColorA with ColorB.
	/// </summary>
	/// <param name="colorA">Color a.</param>
	/// <param name="colorB">Color b.</param>
	public static Color Mix(Color colorA, Color colorB)
	{
		Color newColor = Mix2Colours (1, colorA, 1, colorB);
		return newColor;
	}
	/// <summary>
	/// Returns the avergae of all Colors in array.
	/// </summary>
	/// <param name="colors">Colors.</param>
	public static Color Mix(Color[] colors){
		
		int arrayLength = colors.Length;
		Color newColor  = new Color(0f, 0f, 0f, 0f);
		
		for (int i = 0; i < arrayLength; i++)
        {
			newColor.r += colors[i].r / arrayLength;
			newColor.g += colors[i].g / arrayLength;
			newColor.b += colors[i].b / arrayLength;
			newColor.a += colors[i].a / arrayLength;
		}
		
		return newColor;
	}
	/// <summary>
	/// Returns the aggregate of all Colors in array with integer array to set weight to each Color.
	/// Arrays must be of identical length.
	/// </summary>
	/// <param name="colours">Colours.</param>
	/// <param name="parts">Parts.</param>
	public static Color Mix(Color[] colors, int[] parts){

		int arrayLength = colors.Length;
		int totalParts  = 0;
		Color newColor  = new Color(0f, 0f, 0f, 0f);

		if (colors.Length == parts.Length){
			// Count total parts / ratio
			for (int i = 0; i < arrayLength; i++)
				totalParts += parts[i];

			for (int i = 0; i < arrayLength; i++){
				newColor.r += (colors[i].r * parts[i]) / (arrayLength * totalParts);
				newColor.g += (colors[i].g * parts[i]) / (arrayLength * totalParts);
				newColor.b += (colors[i].b * parts[i]) / (arrayLength * totalParts);
				newColor.a += (colors[i].a * parts[i]) / (arrayLength * totalParts);
			}
		} else {
			Debug.LogError("Mix failed: Array lengths do not match!");
			//If this failed, it returns Color(0, 0, 0, 0) as not color was calculated
		}

		return newColor;
	}
    #endregion

    #region Set Methods (Set Color components)
    /// <summary>
    /// Set the red component of the Color.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="red"></param>
    /// <returns></returns>
    public static Color SetRed(Color color, float red)
    {
        color.r = red;
        return color;
    }
    /// <summary>
    /// Set the green component of the Color.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="green"></param>
    /// <returns></returns>
    public static Color SetGreen(Color color, float green)
    {
        color.g = green;
        return color;
    }
    /// <summary>
    /// Set the blue component of the Color.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="blue"></param>
    /// <returns></returns>
    public static Color SetBlue(Color color, float blue)
    {
        color.b = blue;
        return color;
    }
    /// <summary>
    /// Set the alpha component of the Color.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="alpha"></param>
    /// <returns></returns>
    public static Color SetAlpha(Color color, float alpha)
    {
        color.a = alpha;
        return color;
    }
    /// <summary>
    /// Override a Color by passing in the desired values to change and using null to skip any value.
    /// </summary>
    /// <param name="color">The Color to override</param>
    /// <param name="red">The red value to override. Pass null to skip overriding it.</param>
    /// <param name="green">The green value to override. Pass null to skip overriding it.</param>
    /// <param name="blue">The blue value to override. Pass null to skip overriding it.</param>
    /// <param name="alpha">The alpha value to override. Pass null to skip overriding it.</param>
    /// <returns></returns>
    public static Color SetOverride(Color color, float? red, float? green, float? blue, float? alpha)
    {
        Color newColor = color;

        if (red != null)
            newColor.r = (float)red;

        if (green != null)
            newColor.g = (float)green;

        if (blue != null)
            newColor.b = (float)blue;

        if (alpha != null)
            newColor.a = (float)alpha;

        return newColor;
    }
    #endregion
    #endregion
}
