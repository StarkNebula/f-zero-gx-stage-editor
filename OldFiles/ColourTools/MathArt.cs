﻿using UnityEngine;
using System.Collections;

public static class MathArt
{
    #region Pulsewave Functions
    /// <summary>
    /// Pulse wave ranging from -1 to 1, at a frequency of cyclesPersecond.
    /// </summary>
    /// <param name="cyclesPerSecond"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static float PulseWave(float cyclesPerSecond, float t)
    {
        return (Mathf.Sin((t / cyclesPerSecond) * Mathf.PI * 2f));
    }
    /// <summary>
    /// Pulse wave ranging from -1 to 1, at a frequency of cyclesPersecond.
    /// </summary>
    /// <param name="cyclesPerSecond"></param>
    /// <param name="t"></param>
    /// <param name="power"></param>
    /// <returns>Returns Sin(t / cyclesPerSecond)^power.</returns>
    public static float PulseWave(float cyclesPerSecond, float t, float power)
    {
        return (Mathf.Pow(Mathf.Sin((t / cyclesPerSecond) * Mathf.PI * 2f), power));
    }

    /// <summary>
    /// Pulse wave ranging from 0 to 1, at a frequency of cyclesPersecond.
    /// </summary>
    /// <param name="cyclesPerSecond"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static float PulseWavePositive(float cyclesPerSecond, float t)
    {
        return (1f + Mathf.Sin((t / cyclesPerSecond) * Mathf.PI * 2f)) / 2f;
    }
    /// <summary>
    /// Pulse wave ranging from 0 to 1, at a frequency of cyclesPersecond.
    /// </summary>
    /// <param name="cyclesPerSecond"></param>
    /// <param name="t"></param>
    /// <param name="power"></param>
    /// <returns>Returns Sin(t / cyclesPerSecond)^power.</returns>
    public static float PulseWavePositive(float cyclesPerSecond, float t, float power)
    {
        return Mathf.Pow(1f + Mathf.Sin((t / cyclesPerSecond) * Mathf.PI * 2f) / 2f, power);
    }
    #endregion

    #region Parabola
    /// <summary>
    /// Returns a Parabola [y = x^2]
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public static Vector2 Parabola(float x)
    {
        float y = x * x;
        return new Vector2(x, y);
    }
    #endregion

    /// <summary>
    /// TEST
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="radius"></param>
    /// <param name="position"></param>
    /// <param name="color"></param>
    public static void DrawDebugCircle(Vector3 a, Vector3 b, float radius, Vector3 position, Color color)
    {
        int interval = 9;

        for (int i = 0; i < 360; i += interval)
        {
            // Calculate
            float x1 = Mathf.Cos((float)i * Mathf.Deg2Rad) * radius;
            float y1 = Mathf.Sin((float)i * Mathf.Deg2Rad) * radius;

            float x2 = Mathf.Cos((float)((i + interval) % 360) * Mathf.Deg2Rad) * radius;
            float y2 = Mathf.Sin((float)((i + interval) % 360) * Mathf.Deg2Rad) * radius;

            Debug.DrawLine(
                position + a * x1 + b * y1,
                position + a * x2 + b * y2,
                color);
        }
    }


    /// <summary>
    /// Generates a point on a circle. Time of 0 generates a point at x = 0, y = 1. A cycle takes 1 second to complete and moves clockwise.
    /// </summary>
    /// <param name="a">Scale of x</param>
    /// <param name="b">Scale of y</param>
    /// <param name="tx">Time x</param>
    /// <param name="ty">Time y</param>
    /// <returns>x = a * Cos(tx), y = b * Sin(ty)</returns>
    public static Vector2 Circle(float a, float b, float tx, float ty)
    {
        Vector2 circle = new Vector2(
            // - minus sign makes circle rotate clockwise
            -a * Mathf.Cos(tx * Mathf.Deg2Rad * 360f),
             b * Mathf.Sin(ty * Mathf.Deg2Rad * 360f));

        return circle;
    }


}
