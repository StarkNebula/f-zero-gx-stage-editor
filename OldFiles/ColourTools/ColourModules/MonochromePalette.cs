﻿using UnityEngine;

public class MonochromePalette
{
    // This class's contents were intentionally made virtual so that this
    // could become a base class for monochrome modules to inherit from.

    public virtual int m_depth { get { return 2; } }
    public virtual float m_min { get { return 0.0f; } }
    public virtual float m_max { get { return 1.0f; } }

    public virtual Color black { get { return Palette.BaseShadeAlgorithm(0, m_depth, m_min, m_max); } }
    public virtual Color grey  { get { return Palette.BaseShadeAlgorithm(1, m_depth, m_min, m_max); } }
    public virtual Color white { get { return Palette.BaseShadeAlgorithm(2, m_depth, m_min, m_max); } }

    public virtual Color NewShade(int _select, int _depth)
    {
        return Palette.BaseShadeAlgorithm(_select, _depth, m_min, m_max);
    }
    public virtual Color NewShade(int _select, int _depth, int _min, int _max)
    {
        return Palette.BaseShadeAlgorithm(_select, _depth, _min, _max);
    }
}
