﻿using UnityEngine;

/*
[ExecuteInEditMode]
public class TestScript : MonoBehaviour
{

    public Transform[] trans;
    public int index = 1;
    public float steps = 20f;
    public Vector3[][] points;
    public float alpha = 0.5f;

    void Update()
    {
        Vector3[] pos = new Vector3[trans.Length];
        for (int i = 0; i < trans.Length; i++)
            pos[i] = trans[i].position;

        points = new Vector3[trans.Length][];

        for (int i = 0; i < trans.Length; i++)
            points[i] = SplineInterpolation.CatmullRom2(pos, i, alpha);

        for (int j = 0; j < trans.Length; j++)
            for(int i = 0; i < points[j].Length - 1; i++)
                Debug.DrawLine(points[j][i], points[j][i+1], Palette.ColorWheel(j, 12));
    }

    void OnDrawGizmos()
    {
        for (int i = 0; i < trans.Length; i++)
        {
            Gizmos.color = Palette.ColorWheel(i, 12).SetAlpha(0.33f);
            Gizmos.DrawSphere(trans[i].position, .1f);
        }

        for (int j = 0; j < trans.Length; j++)
            for (int i = 0; i < points[j].Length; i++)
            {
                Gizmos.color = Palette.ColorWheel(i, 12).SetAlpha(0.33f);
                Gizmos.color.SetAlpha(0.33f);
                Gizmos.DrawSphere(points[j][i], 0.005f);
            }
    }
}
*/