﻿using UnityEngine;

public class BasePalette
{
    public virtual float offsetR { get { return 2f; } }
    public virtual float offsetG { get { return 0f; } }
    public virtual float offsetB { get { return 4f; } }
    public const int depth = 24; //{ get { return 24; } }
    public virtual float minSaturation { get { return 0f; } }
    public virtual float maxSaturation { get { return 1f; } }

    public virtual Color red            { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 0, depth, minSaturation, maxSaturation);  } }
    public virtual Color red_orange     { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 1, depth, minSaturation, maxSaturation);  } }
    public virtual Color orange         { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 2, depth, minSaturation, maxSaturation);  } }
    public virtual Color yellow_orange  { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 3, depth, minSaturation, maxSaturation);  } }
    public virtual Color yellow         { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 4, depth, minSaturation, maxSaturation);  } }
    public virtual Color yellow_lime    { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 5, depth, minSaturation, maxSaturation);  } }
    public virtual Color lime           { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 6, depth, minSaturation, maxSaturation);  } }
    public virtual Color lime_green     { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 7, depth, minSaturation, maxSaturation);  } }
    public virtual Color green          { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 8, depth, minSaturation, maxSaturation);  } }
    public virtual Color green_teal     { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 9, depth, minSaturation, maxSaturation);  } }
    public virtual Color teal           { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 10, depth, minSaturation, maxSaturation); } }
    public virtual Color cyan_teal      { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 11, depth, minSaturation, maxSaturation); } }
    public virtual Color cyan           { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 12, depth, minSaturation, maxSaturation); } }
    public virtual Color cyan_cobalt    { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 13, depth, minSaturation, maxSaturation); } }
    public virtual Color cobalt         { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 14, depth, minSaturation, maxSaturation); } }
    public virtual Color cobalt_blue    { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 15, depth, minSaturation, maxSaturation); } }
    public virtual Color blue           { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 16, depth, minSaturation, maxSaturation); } }
    public virtual Color blue_violet    { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 17, depth, minSaturation, maxSaturation); } }
    public virtual Color violet         { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 18, depth, minSaturation, maxSaturation); } }
    public virtual Color magenta_violet { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 19, depth, minSaturation, maxSaturation); } }
    public virtual Color magenta        { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 20, depth, minSaturation, maxSaturation); } }
    public virtual Color magenta_rose   { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 21, depth, minSaturation, maxSaturation); } }
    public virtual Color rose           { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 22, depth, minSaturation, maxSaturation); } }
    public virtual Color rose_red       { get { return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, 23, depth, minSaturation, maxSaturation); } }

    /// <summary>
    /// Returns a color from the palette type.
    /// Specifically selected of depth.
    /// Ex: 2 / 24 = orange.
    /// 3 / 24 = yellow orange.
    /// 4 / 24 = yellow.
    /// </summary>
    /// <param name="_select"></param>
    /// <param name="_depth"></param>
    /// <returns></returns>
    public virtual Color NewColour(int _select, int _depth)
    {
        return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, _select, _depth, minSaturation, maxSaturation);
    }
    /// <summary>
    /// Returns a color from the palette type. This method is useful in for loops.
    /// Specifically selected of depth.
    /// Ex: 2 / 24 = orange.
    /// 3 / 24 = yellow orange.
    /// 4 / 24 = yellow.
    /// /// </summary>
    /// <param name="_select"></param>
    /// <param name="_depth"></param>
    /// <returns></returns>
    public virtual Color ColourWheel(int _select, int _depth)
    {
        // Yes, this method is the same as NewColour().
        // The reasoning is semantics: it makes sense to put ColourWheel() inside a for loop.
        return Palette.BaseColourAlgorith(offsetR, offsetG, offsetB, _select, _depth, minSaturation, maxSaturation);
    }
}
