﻿public class PastelPalette : BasePalette
{
    public override float minSaturation { get { return 0.65f; } }
    public override float maxSaturation { get { return 1.00f; } }
}
