﻿public class DarkPalette : BasePalette
{
    public override float minSaturation { get { return 0.0f; } }
    public override float maxSaturation { get { return 0.4f; } }
}
