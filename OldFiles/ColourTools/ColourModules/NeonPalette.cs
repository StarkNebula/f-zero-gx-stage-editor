﻿public class NeonPalette : BasePalette
{
    public override float minSaturation { get { return 0.2f; } }
    public override float maxSaturation { get { return 1.0f; } }
}
