﻿using UnityEngine;
using System.Collections;

/*
    This is really weird. Looks like a position and rotation, but I can't be totally sure
**/

public class DisplayStoryModeSpecialData : FZGX_UnityEditorEventDrivenClass
{

    public StoryModeSpecialData[] storyModeData;

    public bool drawStartPos;
    public Color color = Palette.white;
    public float gizmosSize = 1f;

    public override void Main()
    {
        storyModeData = StoryModeSpecialData.LoadStoryModeSpecialData();
    }

    public void Update()
    {
    }

    public void OnDrawGizmos()
    {
        for (int i = 0; i < storyModeData.Length; i++)
        {
            // Colliders
            Gizmos.color = Palette.cyan.Whitten(0.5f);
            Gizmos.DrawCube(storyModeData[i].position, storyModeData[i].triggerScale);

            //Objects
            Gizmos.color = Palette.orange.Whitten(0.5f);
            Gizmos.DrawSphere(storyModeData[i].position, gizmosSize);
        }
    }
}
