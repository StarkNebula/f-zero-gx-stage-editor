﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class DisplaySegments : FZGX_UnityEditorEventDrivenClass
{
    public static Material[] sm_mat;
    public Material[] mat;
    [Range(0f, 1f)]
    public float matAlpha = 0.5f;

    public SplineSegment[] distincts;

    //Instantiate gObj instead of using gizmos
    public override void Main()
    {
        UpdateMaterials();

        distincts = LoadDistinctSegmentData();

        transform.ClearTransformImmediate();

        for (int i = 0; i < distincts.Length; i++)
        {
            GameObject temp = distincts[i].Instantiate(i.ToString(), 0);
            temp.transform.parent = this.transform;
        }
    }
    //Draw gizmos directly from data
    public void OnDrawGizmos()
    {
        //RecursivelyDrawSegmentGizmo(distincts, 0);
    }


    public SplineSegment[] LoadDistinctSegmentData()
    {
        SplineSegment[] distinctSegments = SplineData.DistillSegmentData(SplineData.LoadSplineData());
        return distinctSegments;
    }


    private void RecursivelyDrawSegmentGizmo(SplineSegment[] segments, int depth)
    {
        for (int i = 0; i < segments.Length; i++)
        {
            Gizmos.color = Palette.ColorWheel(depth, 10).SetAlpha(0.33f);
            Gizmos.DrawCube(segments[i].GetRecursivePosition(), segments[i].localScale);

            // Recursive draw cubes
            if (segments[i].childCount > 0)
                RecursivelyDrawSegmentGizmo(segments[i].subSegment, depth + 1);
        }
    }
    private void UpdateMaterials()
    {
        for (int i = 0; i < mat.Length; i++)
            mat[i].color = Palette.ColorWheel(i, 12).SetAlpha(matAlpha / i);
        sm_mat = mat;
    }

}
