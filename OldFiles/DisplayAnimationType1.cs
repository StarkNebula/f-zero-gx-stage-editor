﻿using UnityEngine;
using System.Collections;

/*
    This is really weird. Looks like a position and rotation, but I can't be totally sure
**/

public class DisplayAnimationType1 : FZGX_UnityEditorEventDrivenClass
{

    public AnimationType1[] animationType1;

    public bool drawStartPos;
    public Color color = Palette.white;
    public float gizmosSize = 1f;

    public override void Main()
    {
        animationType1 = AnimationType1.LoadAnimationType1();
    }

    public void Update()
    {
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        for (int i = 0; i < animationType1.Length; i++)
        {
            Gizmos.color = color;
            Gizmos.DrawSphere(animationType1[i].position, gizmosSize);
            //Gizmos.DrawSphere(animationType1[i].rotation, gizmosSize);
        }
    }
}
