﻿using UnityEngine;
using System.Collections;

public class DisplayAIEffects : FZGX_UnityEditorEventDrivenClass {

    public AiVisibleEffects[] aiVisibleEffects;

    public override void Main()
    {
        aiVisibleEffects = AiVisibleEffects.LoadAIVisibleEffects();
    }

    public void Update()
    {
        // Nothing to display yet
    }
}
