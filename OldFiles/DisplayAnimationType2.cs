﻿using UnityEngine;
using System.Collections;

/*
    This is really weird. Looks like a position and rotation, but I can't be totally sure
**/

public class DisplayAnimationType2 : FZGX_UnityEditorEventDrivenClass
{

    public AnimationType2[] animationType2;

    public bool drawStartPos;
    public Color color = Palette.white;
    public float gizmosSize = 1f;

    public override void Main()
    {
        animationType2 = AnimationType2.LoadAnimationType2();
    }

    public void Update()
    {
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        for (int i = 0; i < animationType2.Length; i++)
        {
            Gizmos.color = color;
            Gizmos.DrawSphere(animationType2[i].position, gizmosSize);
        }
    }
}
