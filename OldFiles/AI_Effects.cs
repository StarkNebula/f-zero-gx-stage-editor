﻿

/*
 * TODO:
 *  - Have this class poke into spline data and solve the position of each effect.
 *  - Display effect in position, using appropriate colours to display them.
 *  - In the future, incoporate this into the effects placement editor tool.
 * 
*/


using UnityEngine;
using System.IO;

namespace FZGX_StageEditor
{
    public class AI_Effects : FZGX_UnityEditorEventDrivenClass
    {
        AIVisibleEffects[] entry;

        public override void Start()
        {
            entry = AIVisibleEffects.LoadAIVisibleEffects();
        }
    }

    /// <summary>
    /// AI VISIBLE EFECTS (TARGET)
    /// </summary>
    [System.Serializable]
    public struct AIVisibleEffects
    {
        /*/ 0x00 32 /*/ public float splinePositionFrom;
        /*/ 0x04 32 /*/ public float splinePositionTo;
        /*/ 0x08 32 /*/ public float widthLeft;  // VERIFY
        /*/ 0x0C 32 /*/ public float widthRight; // VERIFY
        /*/ 0x10  8 /*/ public byte  effectType; // Effects Collision Type: 0 (FinishLine?), 1 Healing, 2 (?), 3 Boost Pad, 4 Jump
        /*/ 0x11  8 /*/ public byte  trackBranchID; // Track Branch ID (0 if single, 1-2-3...)
        /*/ 0x12 16 /*/ //NULL
        public const uint size = 0x14;

        // CONSTRUCTOR
        public AIVisibleEffects(uint seekOffset)
        {
            Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

            splinePositionFrom = Stage.reader.GetFloat();
            splinePositionTo = Stage.reader.GetFloat();
            widthLeft = Stage.reader.GetFloat();
            widthRight = Stage.reader.GetFloat();
            effectType = Stage.reader.GetByte();
            trackBranchID = Stage.reader.GetByte();
        }

        // METHODS
        /// <summary>
        /// Load multiple AIVisibleEffects at once. It is hard-coded to read the correct
        /// header offset when initializing data.
        /// </summary>
        public static AIVisibleEffects[] LoadAIVisibleEffects()
        {
            HeaderEntry header = new HeaderEntry(Stage.reader, 0x10);

            AIVisibleEffects[] aiVisibleEffects = new AIVisibleEffects[header.count];
            for (uint i = 0; i < header.count; i++)
                aiVisibleEffects[i] = new AIVisibleEffects(header.offset + i * size);

            return aiVisibleEffects;
        }
    }
    public enum AIVisibleEffectType
    {
        FinishLine_UNK = 0,
        Heal  = 1,
        UNK_2 = 2,
        BoostPad = 3,
        JumpPad  = 4,
    }
}