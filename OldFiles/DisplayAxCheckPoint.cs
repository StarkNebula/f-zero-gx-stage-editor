﻿using UnityEngine;
using System.Collections;

/*
    This is really weird. Looks like a position and rotation, but I can't be totally sure
**/

public class DisplayAxCheckPoint : FZGX_UnityEditorEventDrivenClass
{

    public AxCheckpoint[] axCheckpoint;

    public Color color = Palette.white;
    public float gizmosSize = 1f;

    public override void Main()
    {
        axCheckpoint = AxCheckpoint.LoadAxCheckpoint();
    }
    public void Update()
    {
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        for (int i = 0; i < axCheckpoint.Length; i++)
        {
            Gizmos.color = color;
            Gizmos.DrawCube(axCheckpoint[i].globalPosition, axCheckpoint[i].localScale * gizmosSize);
        }
    }
}
