﻿// Resources
//http://answers.unity3d.com/questions/8187/how-can-i-read-binary-files-from-resources.html

using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;

/// <summary>
/// Event that gets triggered when a stage is changed
/// </summary>
[Serializable]
public class StageChangedEvent : UnityEvent { }

/// <summary>
/// Singleton script for controlling stage loading and stream management
/// </summary>
[ExecuteInEditMode]
[Serializable]
public class Stage : MonoBehaviour
{
    [HideInInspector] public static FzgxStage currentStage = FzgxStage.MUTE_CITY_Twist_Road;
    [HideInInspector] public static FzgxStage lastStage = FzgxStage.EX_Victory_Lap;

    // Used to modify how Vector3s are read from file to compensate for difference in winding
    public static bool doInverseWindingPositionX = true;
    public static bool doInverseWindingRotationX = true;
    public static bool doInverseWindingScaleX = false;
    public static bool doInverseWindingNormalX = true;
    public static float alpha = 0.5f;

    // Stage file
    private static TextAsset stageFile;
    private static Stream fileStream;
    public  static BinaryReader reader;

    // Event
    public static StageChangedEvent stageChangedCallback = new StageChangedEvent();
    public static Stage singleton;
    public static Action StageChangedCallback;

    [UnityEditor.Callbacks.DidReloadScripts]
    public static void Reset()
    {
        //Debug.LogWarning("Got callback");
        StageChangedCallback = delegate { Debug.LogFormat("Loaded: ({0}) {1}", ((int)currentStage).ToString("D2"), currentStage.ToString().Replace('_', ' ')); };
    }

    public void Update()
    {
        if (singleton == null)
            singleton = GetComponent<Stage>();

        if (currentStage != lastStage)
        {
            LoadStageFileAndSetStream();
            lastStage = currentStage;
        }
    }

    public void LoadStageFileAndSetStream()
    {
        // Load file based on name. GX stores it's files as COLI_COURSE##,lz. If the number exceeds 99, the number
        // section grows with it. ie: COLI_COURSE###,lz
        //stageFile = Resources.Load("COLI/COLI_COURSE" + ((int)currentStage).ToString("D2") + ",lz") as TextAsset;
        stageFile = Resources.Load(string.Format("COLI/COLI_COURSE{0},lz", ((int)currentStage).ToString("D2"))) as TextAsset;

        // Load file as bytes
        fileStream = new MemoryStream(stageFile.bytes);
        // Set the reader's stream to the loaded file
        reader = new BinaryReader(fileStream, System.Text.Encoding.UTF8);

        // Signal event
        //if (stageChangedCallback != null)
        //stageChangedCallback.Invoke();
        StageChangedCallback.Invoke();

        // Check to see if necessary
        UnityEditor.EditorUtility.SetDirty(singleton);
    }
}