﻿using UnityEngine;
using System.Collections;

public class DisplayPoints : FZGX_UnityEditorEventDrivenClass
{
    private uint[] segOffset; // Referenced for coloring using SetColourBasedOnSegment

    public SplinePoint[] points;
    public bool drawKochanekBartelsSpline = true;
    public bool drawKochanekBartelsSplineTangents = false;
    public float kochanekBartelsTangentLength = 15f;

    public bool drawHermiteSpline = true;
    public bool useSegmentColors = true;

    public bool displayPointTangents = true;
    public float pointTangentsLength = 10f;
    public Color colorTangentA = Palette.rose_red;
    public Color colorTangentB = Palette.cobalt;

    public bool drawGizmos = true;
    public float gizmosSize = 10f;

    [Range(-1f, 1f)]
    public float tension = 0f;
    [Range(-1f, 1f)]
    public float bias = 0f;
    [Range(-1f, 1f)]
    public float continuity = 0f;

    // Update information on Stage Change
    public override void Main()
    {
        points = LoadSplinePoints(SplineData.LoadSplineData());
        segOffset = GetUniqueSegmentOffsets();
    }

    #region Display Debug Information
    public void Update()
    {
        if (drawKochanekBartelsSpline)
            DrawKochanekBartels();

        if (drawHermiteSpline)
            DrawHermite();

        if (displayPointTangents)
            DisplayPointTangents();
    }
    public void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            for (int i = 0; i < points.Length; i++)
            {
                if (useSegmentColors)
                    Gizmos.color = SetColorBasedOnSegment(i);
                else
                    Gizmos.color = Palette.ColorWheel(i, points.Length).SetAlpha(0.5f).Whitten(.5f);

                Gizmos.DrawSphere(points[i].startPosition, gizmosSize);
                Gizmos.DrawWireSphere(points[i].endPosition, gizmosSize);
            }
        }
    }
    #endregion

    private SplinePoint[] LoadSplinePoints(SplineData[] splineData)
    {
        SplinePoint[] newPoints = new SplinePoint[splineData.Length];

        for (int i = 0; i < splineData.Length; i++)
            newPoints[i] = splineData[i].point;

        return newPoints;
    }
    private uint[] GetUniqueSegmentOffsets()
    {
        SplineSegment[] segments = SplineData.DistillSegmentData( SplineData.LoadSplineData() );

        uint[] offsets = new uint[segments.Length];

        for (int i = 0; i < segments.Length; i++)
            offsets[i] = segments[i].thisSegmentOffset;

        return offsets;
    }
    private Color SetColorBasedOnSegment(int i)
    {
        // Find index
        int index = 0;
        for (int j = 0; j < segOffset.Length; j++)
            if (segOffset[j] == points[i].segmentOffset)
                index = j;

        // Set color releative to index
        Color color = Palette.ColorWheel(index, segOffset.Length);
        if ((index % 2) != 0)
        {
            // every other color
            color = color.Whitten(.75f);
            // Makes it easy to differentiate between segments more
        }

        return color;
    }

    private void DrawHermite()
    {
        // Draw Hermite Spline along points
        for (int i = 0; i < points.Length; i++)
        {
            Color color = Palette.white;
            if (useSegmentColors)
                color = SetColorBasedOnSegment(i);

            for (int j = 0; j < 20; j++)
                Debug.DrawLine(
                    SplineInterpolation.HermiteSpline(
                        points[i].startPosition,
                        points[i].endPosition,
                        points[i].startTangent,
                        -points[i].endTangent,
                        j, 20),

                    SplineInterpolation.HermiteSpline(
                        points[i].startPosition,
                        points[i].endPosition,
                        points[i].startTangent,
                        -points[i].endTangent,
                        j + 1, 20),

                    //Palette.ColorWheel(i, splineData.Length).SetAlpha(0.25f).Whitten(0.5f));
                    color);
                    //Color.white.SetAlpha(0.33f));
        }
    }
    private void DrawKochanekBartels()
    {
        Vector3[] startPositions = new Vector3[points.Length];
        Vector3[] endPositions   = new Vector3[points.Length];
        for(int i = 0; i < points.Length; i++)
        {
            startPositions[i] = points[i].startPosition;
            endPositions[i]   = points[i].endPosition;
        }

        for (int i = 0; i < points.Length; i++)
        {
            // GET KB list of points
            Vector3[] kbSplinePoints = FzgxSplineInter.KochanekBartels(
                startPositions,
                endPositions,
                i,
                points[i].mergeFromLast,
                points[i].mergeToNext,
                tension,
                bias,
                continuity,
                drawKochanekBartelsSplineTangents,
                kochanekBartelsTangentLength
                );

            Color color = Palette.white;

            if (useSegmentColors)
                color = SetColorBasedOnSegment(i);
            else
                Palette.ColorWheel(i, points.Length).SetAlpha(0.5f);

            // Lerp for KB points
            for (int j = 0; j < kbSplinePoints.Length - 1; j++)
                Debug.DrawLine(kbSplinePoints[j], kbSplinePoints[j + 1], color);
            // Draw line to end
            Debug.DrawLine(kbSplinePoints[kbSplinePoints.Length - 1], points[i].endPosition, color);

        }
    }
    private void DisplayPointTangents()
    {
        for (int i = 0; i < points.Length; i++)
        {
            Debug.DrawLine(points[i].startPosition, points[i].startPosition + points[i].startTangent * pointTangentsLength, colorTangentA);
            Debug.DrawLine(points[i].endPosition,   points[i].endPosition   + points[i].endTangent   * pointTangentsLength, colorTangentB);
        }
    }
}
