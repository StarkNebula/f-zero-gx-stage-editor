﻿using UnityEngine;

/// <summary>
/// Notice: This class uses [ExecuteInEditMode]
/// </summary>
[ExecuteInEditMode]
public class FZGX_UnityEditorEventDrivenClass : MonoBehaviour {

    public virtual void Start()
    {
        Stage.stageChangedCallback.AddListener(Main);
    }
    public virtual void OnEnable()
    {
        // Add listener to UnityEvent
        Stage.stageChangedCallback.AddListener(Main);
    }
    public virtual void OnDisable()
    {
        // Remove listener to UnityEvent
        Stage.stageChangedCallback.RemoveListener(Main);
    }
    public virtual void OnDestroy()
    {
        // Remove listener to UnityEvent
        Stage.stageChangedCallback.RemoveListener(Main);
    }

    public virtual void Main ()
    {
        //Debug.Log("Event successful");
	}
}
