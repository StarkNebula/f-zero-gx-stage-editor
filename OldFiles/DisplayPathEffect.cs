﻿using UnityEngine;
using System.Collections;

/*
    This is really weird. Looks like a position and rotation, but I can't be totally sure
**/

public class DisplayPathEffect : FZGX_UnityEditorEventDrivenClass
{

    public EffectPath[] effectPath;

    public bool drawStartPos;
    public Color color = Palette.white;
    public float gizmosSize = 1f;

    public override void Main()
    {
        effectPath = EffectPath.LoadEffectPath();
    }

    public void Update()
    {
        for (int i = 0; i < effectPath.Length; i++)
            Debug.DrawLine(effectPath[i].pathPositionStart, effectPath[i].pathPositionEnd, color);
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        for (int i = 0; i < effectPath.Length; i++)
        {
            Gizmos.color = color.SetAlpha(0.5f);
            Gizmos.DrawSphere(effectPath[i].pathPositionStart, gizmosSize);
            Gizmos.color = color;
            Gizmos.DrawWireSphere(effectPath[i].pathPositionEnd, gizmosSize);
        }
    }
}
