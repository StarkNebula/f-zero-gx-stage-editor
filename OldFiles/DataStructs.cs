﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

// Notes from past headaches:
// ALWAYS CHECK FOR NULL POINTERS/OFFSETS!
// MAKE FOR FUCKING SURE THE THE SIZE UINT IN STRUCTS IS CORRECT LENGTH


#region Spline Data
/// <summary>
/// SPLINE OFFSET
/// Count:  0x08
/// Offset: 0x0C
/// </summary>
[System.Serializable]
public struct SplineData
{
    /*/ 0x00 32 /*/ public uint TrackSplitType;
    /*/ 0x04 32 /*/ public uint PointOffset;
    /*/ 0x08 32 /*/ public uint SegmentOffset;
    public const uint size = 0x0C;
    public const uint g_count = 0x08;
    public const uint g_offset = 0x0C;

    public SplinePoint point;
    public SplineSegment segment;

    public SplineData(uint seekPosition)
    {
        Stage.reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);

        TrackSplitType  = FormatBytesAs.UInt32(Stage.reader);
        PointOffset     = FormatBytesAs.UInt32(Stage.reader);
        SegmentOffset   = FormatBytesAs.UInt32(Stage.reader);

        point   = new SplinePoint(PointOffset, SegmentOffset);
        segment = new SplineSegment(SegmentOffset, null);
    }

    /// <summary>
    /// Method that returns an array of SplineData.
    /// It will automatically load from the current Stage.reader at address 0x08 in the file header
    /// </summary>
    /// <returns></returns>
    public static SplineData[] LoadSplineData()
    {
        Stage.reader.BaseStream.Seek(0x08, SeekOrigin.Begin);
        uint count = FormatBytesAs.UInt32(Stage.reader);
        uint offset = FormatBytesAs.UInt32(Stage.reader);

        SplineData[] splineData = new SplineData[count];

        for (uint i = 0; i < count; i++)
            splineData[i] = new SplineData(offset + i * SplineData.size);

        return splineData;
    }
    /// <summary>
    /// Returns the distinct splinesegments of splinedata
    /// </summary>
    /// <param name="splineData"></param>
    /// <returns></returns>
    public static SplineSegment[] DistillSegmentData(SplineData[] splineData)
    {
        // Make a list to store all unique entries. At first, it will have duplicates in it.
        List<SplineSegment> segments = new List<SplineSegment>();

        // Get all references
        for (int i = 0; i < splineData.Length; i++)
            segments.Add(splineData[i].segment);

        // Array that will tell us which entries are duplicates
        bool[] deleteThisEntry = new bool[segments.Count];

        // Then, for each entry in the list
        for (int i = 0; i < segments.Count; i++)
        {
            // If this entry has not been flag for deletion
            if (!deleteThisEntry[i])
            {
                // Look at next entry after i, hence j = i+1
                for (int j = (i + 1); j < segments.Count; j++)
                {
                    // if the data matches (found a duplicate), flag for deletion
                    if (segments[j].childOffset == segments[i].childOffset)
                    {
                        deleteThisEntry[j] = true;
                    }
                }
            }
        }

        // Delete entries
        // Made an int here because I can't reference the list size if it keeps getting smaller as I remove items
        int segmentCount = segments.Count;
        for (int i = 0; i < segmentCount; i++)
        {
            // Work our way backwards in the array
            // That's very important!
            int j = segmentCount - i - 1;
            if (deleteThisEntry[j])
                segments.RemoveAt(j);
        }

        // Dump distilled List into array
        SplineSegment[] distinctSegments = new SplineSegment[segments.Count];
        for (int i = 0; i < segments.Count; i++)
            distinctSegments[i] = segments[i];

        return distinctSegments;
    }
}

/// <summary>
/// SPLINE POINT
/// </summary>
[System.Serializable]
public struct SplinePoint
{
    /*/ 0x00 32 /*/ public float   segmentStartReference;
    /*/ 0x04 32 /*/ public float   segmentEndReference;
    /*/ 0x08 32 /*/ public float   startingtPositionAlongSpline;
    /*/ 0x0c 96 /*/ public Vector3 startTangent;
    /*/ 0x18 96 /*/ public Vector3 startPosition;
    /*/ 0x24 96 /*/ public float   endingPositionAlongSpline;
    /*/ 0x28 96 /*/ public Vector3 endTangent;
    /*/ 0x34 96 /*/ public Vector3 endPosition;
    /*/ 0x40 32 /*/ public float   lengthStart;
    /*/ 0x44 32 /*/ public float   lengthEnd;
    /*/ 0x48 32 /*/ public float   splinePointWidth; // Looks like an editor value, since changing it has no apparent effect
    /*/ 0x4C  8 /*/ public bool    mergeFromLast;
    /*/ 0x4D  8 /*/ public bool    mergeToNext;
    /*/ 0x4E 16 /*/ //NULL
    public const uint size = 0x50;

    //Easy reference to segments
    public uint segmentOffset;

    public SplinePoint(uint seekPosition, uint segmentOffset)
    {
        this.segmentOffset = segmentOffset;

        Stage.reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);

        segmentStartReference = FormatBytesAs.Float(Stage.reader);
        segmentEndReference   = FormatBytesAs.Float(Stage.reader);
        startingtPositionAlongSpline = FormatBytesAs.Float(Stage.reader);
        startTangent  = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingNormalX);
        startPosition = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        endingPositionAlongSpline    = FormatBytesAs.Float(Stage.reader);
        endTangent  = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingNormalX);
        endPosition = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        lengthStart = FormatBytesAs.Float(Stage.reader);
        lengthEnd   = FormatBytesAs.Float(Stage.reader);
        splinePointWidth = FormatBytesAs.Float(Stage.reader); // Looks like an editor value, since changing it has no apparent effect
        mergeFromLast    = FormatBytesAs.Bool08(Stage.reader);
        mergeToNext      = FormatBytesAs.Bool08(Stage.reader);
        FormatBytesAs.SkipBytes(Stage.reader, 2);
    }
}

/// <summary>
/// LOGICAL ERROR WITH 
/// SPLINE SEGMENT INFORMATION
/// </summary>
//[System.Serializable]
public struct SplineSegment
{
	/*/ 0x00 16 /*/ public ushort  hierarchyPositionFlag; // 0200 = bottom level, 0400 = parent of level 1's, 0800 = parent of level 2's, etc
	/*/ 0x02 16 /*/ public ushort  childrenFlag; //Children == [0000], No Children == [0C00]
	/*/ 0x04 32 /*/ public uint    libraryOffset;
	/*/ 0x08 32 /*/ //NULL
	/*/ 0x0C 32 /*/ public uint    childCount;
	/*/ 0x10 32 /*/ public uint    childOffset; // Has something to do with spline branches
	/*/ 0x14 96 /*/ public Vector3 localScale;
	/*/ 0x20 96 /*/ public Vector3 localRotation;
	/*/ 0x2C 96 /*/ public Vector3 localPosition;
	/*/ 0x38 32 /*/ public uint    unk_id; // Power of Two ID
	/*/ 0x3C 32 /*/ public float   unk0x3C; // 0x3C and 0x40 are typically the same
	/*/ 0x40 32 /*/ public float   unk0x40;
    /*/ 0x44 96 /*/ //NULL (VECTOR?)
    public const uint size = 0x50;

    public SplineLibrary library;
    public SplineSegment[] subSegment;

    //Parent reference
    public SplineSegment[] parent;

    // This offset for data
    public uint thisSegmentOffset;

    public SplineSegment(uint seekPosition, SplineSegment? parent)
    {
        thisSegmentOffset = seekPosition;

        Stage.reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);

        hierarchyPositionFlag = FormatBytesAs.UInt16(Stage.reader); 
        childrenFlag = FormatBytesAs.UInt16(Stage.reader);
        libraryOffset = FormatBytesAs.UInt32(Stage.reader);
        /*/ NULL 32 /*/ FormatBytesAs.SkipBytes(Stage.reader, 4);
        childCount  = FormatBytesAs.UInt32(Stage.reader);
        childOffset = FormatBytesAs.UInt32(Stage.reader);
        localScale    = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
        localRotation = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingRotationX);
        localPosition = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk_id   = FormatBytesAs.UInt32(Stage.reader);
        unk0x3C  = FormatBytesAs.Float(Stage.reader);
        unk0x40  = FormatBytesAs.Float(Stage.reader);
        //*/ NULL 96 /*/ FormatBytesAs.SkipBytes(Stage.reader, 12);

        library = new SplineLibrary(libraryOffset);

        // If exists, set it, else, set it to null (no parent)
        if (parent != null)
            this.parent = new SplineSegment[] { (SplineSegment)parent };
        else
            this.parent = null;

        #region RECURSIVE SEGMENT LOADING
        if (childCount > 0)
        {
            // Create new sub segment
            subSegment = new SplineSegment[childCount];
            for (int i = 0; i < childCount; i++)
                subSegment[i] = new SplineSegment(childOffset + size * (uint)i, this);
                // + size is to offset pointer to next entry when more than 1
        }
        else
        {
            // Remains empty
            subSegment = new SplineSegment[0];
        }
        #endregion
    }

    //LOAD POSITION VALUE
    public Vector3 GetRecursivePosition()
    {
        Vector3 combinedPosition = Vector3.zero;
        combinedPosition = this.localPosition;

        if (parent != null)
            combinedPosition += parent[0].GetRecursivePosition();

        //Debug.Log(combinedPosition);

        return combinedPosition;
    }
    /* public Vector3 GetRecursiveScale()
    {
        Vector3 combinedScale = Vector3.zero;
        combinedScale = this.scale;

        if (parent != null)
            combinedScale += parent[0].GetRecursiveScale();

        return combinedScale;
    }*/
    //
    public void DrawLineToParent(Color color)
    {
        if (parent != null)
            Debug.DrawLine(this.GetRecursivePosition(), parent[0].GetRecursivePosition(), color);
    }
    public void RecursiveDrawLineToParent(Color color)
    {
        if (parent != null)
        {
            Debug.DrawLine(this.GetRecursivePosition(), parent[0].GetRecursivePosition(), color);
            parent[0].RecursiveDrawLineToParent(color);
        }

    }
    public void RecursiveDrawLineToChildren(Color color)
    {
        for (int i = 0; i < subSegment.Length; i++)
        {
            Debug.DrawLine(this.GetRecursivePosition(), subSegment[i].GetRecursivePosition(), color);
            subSegment[i].RecursiveDrawLineToChildren(color);
        }
    }

    public GameObject Instantiate(string id, int intID)
    {
        //GameObject seg = new GameObject();
        GameObject seg = GameObject.CreatePrimitive(PrimitiveType.Cube);
        seg.transform.localPosition = localPosition;
        seg.transform.localRotation = Quaternion.Euler(localRotation);
        seg.transform.localScale    = localScale;

        seg.name = id;
        seg.GetComponent<MeshRenderer>().sharedMaterial = DisplaySegments.sm_mat[intID];

        for (int j = 0; j < childCount; j++)
        {
            GameObject newSeg = subSegment[j].Instantiate(id + "_" + (j).ToString(), intID + 1);
            newSeg.transform.parent = seg.transform;

            // Reset transform
            newSeg.transform.localPosition = subSegment[j].localPosition;
            newSeg.transform.localRotation = Quaternion.Euler(subSegment[j].localRotation);
            newSeg.transform.localScale    = subSegment[j].localScale;
        }

        return seg;
    }
}

/// <summary>
/// LIBRARY
/// </summary>
//[System.Serializable]
public struct SplineLibrary
{
    /*/ 0x00 32 * 9 /*/ public uint[] entryCount; // Int temp. to fix issues. Not all paths return uint
    /*/ 0x24 32 * 9 /*/ public uint[] entryOffset; //same
    public const uint size = 0x48;
    public SplineLibraryEntry[][] entry;

    public SplineLibrary(uint seekPosition)
    {
        entryCount  = new uint[9];
        entryOffset = new uint[9];
        entry = new SplineLibraryEntry[9][];

        // MAKE SURE TO GUARD DATA FROM NULL OFFSETS! SET STREAM AFTER CHECK ONLY!!!
        if (seekPosition != 0)
        {
            //Debug.Log(seekPosition);
            Stage.reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);
            for (uint i = 0; i < 9; i++) entryCount[i]  = FormatBytesAs.UInt32(Stage.reader);
            for (uint i = 0; i < 9; i++) entryOffset[i] = FormatBytesAs.UInt32(Stage.reader);

            for (uint i = 0; i < 9; i++)
            {
                entry[i] = new SplineLibraryEntry[entryCount[i]];

                for (uint j = 0; j < entry[i].Length; j++)
                    if (entryOffset[i] != 0)
                        entry[i][j] = new SplineLibraryEntry(entryOffset[i]);
            }
        }
    }
}

/// <summary>
/// ENTRY
/// </summary>
[System.Serializable]
public struct SplineLibraryEntry
{
	/*/ 0x00 32 /*/ public uint  id; // 1 or 2
	/*/ 0x04 32 /*/ public float identifier;
	/*/ 0x08 32 /*/ public float variableA;
	/*/ 0x0C 32 /*/ public float variableB;
	/*/ 0x10 32 /*/ public float variableC;
    public const uint size = 0x14;

    public SplineLibraryEntry(uint seekPosition)
    {
        Stage.reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);

        id = FormatBytesAs.UInt32(Stage.reader);
        identifier = FormatBytesAs.Float(Stage.reader);
        variableA  = FormatBytesAs.Float(Stage.reader);
        variableB  = FormatBytesAs.Float(Stage.reader);
        variableC  = FormatBytesAs.Float(Stage.reader);
    }
}
#endregion

/// <summary>
/// AI VISIBLE EFECTS (TARGET)
/// </summary>
[System.Serializable]
public struct AiVisibleEffects
{
    /*/ 0x00 32 /*/ public float splinePositionFrom;
    /*/ 0x04 32 /*/ public float splinePositionTo;
    /*/ 0x08 32 /*/ public float widthLeft;  // VERIFY
    /*/ 0x0C 32 /*/ public float widthRight; // VERIFY
    /*/ 0x10  8 /*/ public byte  effectType; // Effects Collision Type: 0 (FinishLine?), 1 Healing, 2 (?), 3 Boost Pad, 4 Jump
    /*/ 0x11  8 /*/ public byte  trackBranchID; // Track Branch ID (0 if single, 1-2-3...)
    /*/ 0x12 16 /*/ //NULL
    public const uint size = 0x14;

    public AiVisibleEffects(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        splinePositionFrom = FormatBytesAs.Float(Stage.reader);
        splinePositionTo = FormatBytesAs.Float(Stage.reader);
        widthLeft = FormatBytesAs.Float(Stage.reader);
        widthRight = FormatBytesAs.Float(Stage.reader);
        effectType = FormatBytesAs.Byte(Stage.reader);
        trackBranchID = FormatBytesAs.Byte(Stage.reader);
    }

    public static AiVisibleEffects[] LoadAIVisibleEffects()
    {
        Stage.reader.BaseStream.Seek(0x10, SeekOrigin.Begin);
        uint count  = FormatBytesAs.UInt32(Stage.reader); //0x10
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0x14

        AiVisibleEffects[] aiVisibleEffects = new AiVisibleEffects[count];
        for (uint i = 0; i < count; i++)
            aiVisibleEffects[i] = new AiVisibleEffects(offset + i * size);

        return aiVisibleEffects;
    }
}
public enum EffectType
{
    FinishLine_UNK = 0,
    Heal  = 1,
    UNK_2 = 2,
    BoostPad = 3,
    JumpPad  = 4,
}

/// <summary>
/// F-ZERO GX OBJECT
/// Count:  0x
/// Offset: 0x48
/// </summary>
[System.Serializable]
public struct FzgxObject
{
    // IIRC, this is an object prefab
    // Using the last index we get to all the instances of it in the scene
    //
    /*/ 0x00 32 /*/ public uint id1; // ID? - Varies A LOT, but seems to be 4 IDs
    /*/ 0x04 32 /*/ public uint id2; // FFFF FFFF or 8000 0000?
    /*/ 0x08 32 /*/ public uint lodAndCollisionOffset; // Offset to the collision for this object - part of 0x64 header
    /*/ 0x0C 32 /*/ public Vector3 position; // Confirmed position x y z
    /*/ 0x18 32 /*/ public string unknown0x18; // ?
    /*/ 0x1C 32 /*/ public string unknown0x1C; // ID?
    /*/ 0x20 32 /*/ public Vector3 scale; // Confirmed scale x y z
    /*/ 0x2C 32 /*/ //NULL 32
    /*/ 0x30 32 /*/ public uint animationOffset; // Offset 2
    /*/ 0x34 32 /*/ public uint unknown0x34Offset; // Offset 3
    /*/ 0x38 32 /*/ public uint skeletonOffset; // Offset 4 - SKL models
    /*/ 0x3C 32 /*/ public uint transformOffset; // Transform: scale, rotation, position
    public const uint size = 0x40;

    public FzgxObject(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        id1 = FormatBytesAs.UInt32(Stage.reader);
        id2 = FormatBytesAs.UInt32(Stage.reader);
        lodAndCollisionOffset = FormatBytesAs.UInt32(Stage.reader);
        position = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unknown0x18 = FormatBytesAs.String(Stage.reader);
        unknown0x1C = FormatBytesAs.String(Stage.reader);
        scale = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
        /*/ NULL 32 /*/   FormatBytesAs.SkipBytes(Stage.reader, 4);
        animationOffset = FormatBytesAs.UInt32(Stage.reader);
        unknown0x34Offset = FormatBytesAs.UInt32(Stage.reader);
        skeletonOffset =  FormatBytesAs.UInt32(Stage.reader);
        transformOffset = FormatBytesAs.UInt32(Stage.reader);
    }
}

#region Collision
/// <summary>
/// SOLS ONLY
/// Offset: 0x60
/// </summary>
[System.Serializable]
public struct SolsOnlyCollision
{
    /*/ 0x00 32 /*/ public uint lodAndCollisionOffset;
    /*/ 0x04 96 /*/ public Vector3 position;
    /*/ 0x10 32 /*/ public uint unk_offset;
    /*/ 0x14 32 /*/ //NULL 32 ???
    /*/ 0x18 96 /*/ public Vector3 scale;
    public const uint size = 0x24;

    public SolsOnlyCollision(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        lodAndCollisionOffset = FormatBytesAs.UInt32(Stage.reader);
        position = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX); ;
        unk_offset = FormatBytesAs.UInt32(Stage.reader);
        /*/ NULL 32 /*/ FormatBytesAs.SkipBytes(Stage.reader, 4);
        scale = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
    }
}

/// <summary>
/// LOD AND COLLISION MESH
/// Links to: SOLS ONLY
/// Count:  0x64
/// Offset: 0x68
/// </summary>
[System.Serializable]
public struct LodAndCollisionMesh
{
    /*/ 0x00 /*/ public uint LOD1; // LOD related
    /*/ 0x04 /*/ public uint LOD2; // Higher than 1 when prev. value is not 0
    /*/ 0x08 /*/ public uint supplimentaryInfoOffset; // SOLS only
    /*/ 0x0C /*/ public uint collisionDataOffset; // if != 0, is offset?
    public const uint size = 0x10;

    public LodAndCollisionMesh(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        LOD1 = FormatBytesAs.UInt32(Stage.reader);
        LOD2 = FormatBytesAs.UInt32(Stage.reader);
        supplimentaryInfoOffset = FormatBytesAs.UInt32(Stage.reader);
        collisionDataOffset     = FormatBytesAs.UInt32(Stage.reader);
    }
}

/// <summary>
/// SUPPLIMENTARY COLLISION INFORMATION
/// </summary>
[System.Serializable]
public struct CollisionSupplimentaryInfo
{
    /*/ 0x00 32 /*/ //NULL
    /*/ 0x04 32 /*/ public uint nameEntriesOffset;
    /*/ 0x08 32 /*/ //NULL
    /*/ 0x0C 32 /*/ public float LOD_UNK;
    public const uint size = 0x10;

    public CollisionSupplimentaryInfo(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        /*/ NULL 32 /*/ FormatBytesAs.SkipBytes(Stage.reader, 4);
        nameEntriesOffset = FormatBytesAs.UInt32(Stage.reader);
        /*/ NULL 32 /*/ FormatBytesAs.SkipBytes(Stage.reader, 4);
        LOD_UNK = FormatBytesAs.Float(Stage.reader);
    }
}

/// <summary>
/// 
/// </summary>
[System.Serializable]
public struct CollisionData
{
    /*/ 0x00 32 /*/ public uint id;
    /*/ 0x04 32 /*/ public uint unk_04; // Tri?
    /*/ 0x08 32 /*/ public uint unk_08; // Quad?
    /*/ 0x0C 32 /*/ public uint unk_0c; // Tri?
    /*/ 0x10 32 /*/ public uint unk_10; // Quad?
    /*/ 0x14 32 /*/ public uint triangleEntriesCount;
    /*/ 0x18 32 /*/ public uint quadEntriesCount;
    /*/ 0x1C 32 /*/ public uint triangleEntriesOffset;
    /*/ 0x20 32 /*/ public uint quadEntriesOffset;
    public const uint size = 0x24;

    public CollisionData(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        id = FormatBytesAs.UInt32(Stage.reader);
        unk_04 = FormatBytesAs.UInt32(Stage.reader);
        unk_08 = FormatBytesAs.UInt32(Stage.reader);
        unk_0c = FormatBytesAs.UInt32(Stage.reader);
        unk_10 = FormatBytesAs.UInt32(Stage.reader);
        triangleEntriesCount = FormatBytesAs.UInt32(Stage.reader);
        quadEntriesCount = FormatBytesAs.UInt32(Stage.reader);
        triangleEntriesOffset = FormatBytesAs.UInt32(Stage.reader);
        quadEntriesOffset = FormatBytesAs.UInt32(Stage.reader);
    }

}

/// <summary>
/// Triangle Polygon
/// </summary>
[System.Serializable]
public struct Triangle
{
    /*/ 0x00 32 /*/ public float unk_0x00;
    /*/ 0x04 96 /*/ public Vector3 normal;
    /*/ 0x10 96 /*/ public Vector3 vertex1;
    /*/ 0x1C 96 /*/ public Vector3 vertex2;
    /*/ 0x28 96 /*/ public Vector3 vertex3;
    /*/ 0x34 96 /*/ public Vector3 unk1;
    /*/ 0x40 96 /*/ public Vector3 unk2;
    /*/ 0x4C 96 /*/ public Vector3 unk3;
    public const uint size = 0x58;

    public Triangle(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        unk_0x00 = FormatBytesAs.Float(Stage.reader);
        normal  = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingNormalX);
        vertex1 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        vertex2 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        vertex3 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk1 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk2 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk3 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
    }
}

/// <summary>
/// Quad Polygon
/// </summary>
[System.Serializable]
public struct Quad
{
    /*/ 0x00 32 /*/ public float unk_0x00;
    /*/ 0x04 96 /*/ public Vector3 normal;
    /*/ 0x10 96 /*/ public Vector3 vertex1;
    /*/ 0x1C 96 /*/ public Vector3 vertex2;
    /*/ 0x28 96 /*/ public Vector3 vertex3;
    /*/ 0x34 96 /*/ public Vector3 vertex4;
    /*/ 0x40 96 /*/ public Vector3 unk1;
    /*/ 0x4C 96 /*/ public Vector3 unk2;
    /*/ 0x58 96 /*/ public Vector3 unk3;
    /*/ 0x64 96 /*/ public Vector3 unk4;
    public const uint size = 0x68;

    public Quad(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        unk_0x00 = FormatBytesAs.Float(Stage.reader);
        normal = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingNormalX);
        vertex1 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        vertex2 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        vertex3 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        vertex4 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk1 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk2 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk3 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        unk4 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
    }
}
#endregion

#region Special Data
/// <summary>
/// 
/// Count: # of entries = gameobjects with not 0 LOD
/// Offset: 0x84
/// </summary>
[System.Serializable]
public struct LodObjects
{
    /*/ 0x00 96 /*/ public Vector3 position1;
    /*/ 0x0C 32 /*/ public uint unk_0x0C;
    /*/ 0x10 32 /*/ public uint unk_0x10;
    /*/ 0x14 96 /*/ public Vector3 position2;
    /*/ 0x20 32 /*/ public uint potId;
    public const uint size = 0x24;

    public LodObjects(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        position1 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        //*/ Skip 64 /*/ FormatBytesAs.SkipBytes(Stage.reader, 8);
        unk_0x0C = FormatBytesAs.UInt32(Stage.reader);
        unk_0x10 = FormatBytesAs.UInt32(Stage.reader);
        position2 = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        potId = FormatBytesAs.UInt32(Stage.reader);
    }

    /// <summary>
    /// Count: # of entries = gameobjects with not 0 LOD
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public static LodObjects[] LoadLodObjects(uint count)
    {
        Stage.reader.BaseStream.Seek(0x84, SeekOrigin.Begin);
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0x84

        LodObjects[] lodObjects = new LodObjects[count];
        for (uint i = 0; i < count; i++)
            lodObjects[i] = new LodObjects(offset + i * size);

        return lodObjects;
    }

}

/// <summary>
/// 
/// Count:  0x94
/// Offset: 0x98
/// </summary>
[System.Serializable]
public struct AnimationType1
{
    /*/ 0x00 96 /*/ public Vector3 position;
    /*/ 0x0C 32 /*/ public string id_1;
    /*/ 0x10 32 /*/ public string id_2;
    /*/ 0x14 96 /*/ public Vector3 rotation;
    /*/ 0x20 32 /*/ public uint potId; // Lightning: 1, BBO: 4, OSMS: 16
    public const uint size = 0x24;

    public AnimationType1(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        position = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        id_1 = FormatBytesAs.HexString(Stage.reader);
        id_2 = FormatBytesAs.HexString(Stage.reader);
        rotation = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingRotationX);
        potId = FormatBytesAs.UInt32(Stage.reader);
    }
    public static AnimationType1[] LoadAnimationType1()
    {
        Stage.reader.BaseStream.Seek(0x94, SeekOrigin.Begin);
        uint count  = FormatBytesAs.UInt32(Stage.reader); //0x94
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0x98

        AnimationType1[] animationType1 = new AnimationType1[count];
        for (uint i = 0; i < count; i++)
            animationType1[i] = new AnimationType1(offset + i * size);

        return animationType1;
    }
}

/// <summary>
/// 
/// Count:  0x9C
/// Offset: 0xA0
/// </summary>
[System.Serializable]
public struct AnimationType2
{
    /*/ 0x00 96 /*/ public Vector3 position;
    /*/ 0x0C 32 /*/ public uint unk_1;
    /*/ 0x10 32 /*/ public uint unk_2;
    /*/ 0x14 96 /*/ public Vector3 rotation;//?
    /*/ 0x20 32 /*/ public uint id;
    public const uint size = 0x24;

    public AnimationType2(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        position = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        //*/ Skip 64 /*/ FormatBytesAs.SkipBytes(Stage.reader, 8);
        unk_1 = FormatBytesAs.UInt32(Stage.reader);
        unk_2 = FormatBytesAs.UInt32(Stage.reader);
        rotation = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        id = FormatBytesAs.UInt32(Stage.reader);
    }
    public static AnimationType2[] LoadAnimationType2()
    {
        Stage.reader.BaseStream.Seek(0x9C, SeekOrigin.Begin);
        uint count  = FormatBytesAs.UInt32(Stage.reader); //0x9C
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0xA0

        AnimationType2[] animationType2 = new AnimationType2[count];
        for (uint i = 0; i < count; i++)
            animationType2[i] = new AnimationType2(offset + i * size);

        return animationType2;
    }
}

/// <summary>
/// 
/// Count:  0xA4
/// Offset: 0xA8
/// </summary>
[System.Serializable]
public struct  EffectPath
{
    /*/ 0x00 96 /*/ public Vector3 pathPositionStart;
    /*/ 0x0C 32 /*/ public string unk_0x0C;
    /*/ 0x10 32 /*/ public string null_0x10;// CONFIRMED NULL! :D
    /*/ 0x14 96 /*/ public Vector3 pathPositionEnd;//?
    /*/ 0x20 32 /*/ public uint id;
    public const uint size = 0x24;

    public EffectPath(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        pathPositionStart = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        //*/ Skip 64 /*/ FormatBytesAs.SkipBytes(Stage.reader, 8);
        unk_0x0C = FormatBytesAs.HexString(Stage.reader);
        null_0x10 = FormatBytesAs.HexString(Stage.reader);
        pathPositionEnd = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        id = FormatBytesAs.UInt32(Stage.reader);
    }
    public static EffectPath[] LoadEffectPath()
    {
        Stage.reader.BaseStream.Seek(0xA4, SeekOrigin.Begin);
        uint count  = FormatBytesAs.UInt32(Stage.reader); //0xA4
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0xA8

        EffectPath[] effectPath = new EffectPath[count];
        for (uint i = 0; i < count; i++)
            effectPath[i] = new EffectPath(offset + i * size);

        return effectPath;
    }
}

/// <summary>
/// 
/// Count:  0xAC
/// Offset: 0xB0
/// </summary>
[System.Serializable]
public struct AxCheckpoint
{
    /*/ 0x00 96 /*/ public Vector3 globalPosition;
    /*/ 0x0C 32 /*/ public string unk_0x0C;
    /*/ 0x10 32 /*/ public string unk_0x10;
    /*/ 0x14 96 /*/ public Vector3 localScale;
    /*/ 0x20 32 /*/ public uint trackBranchID; // PTCW: 1
    public const uint size = 0x24;

    public AxCheckpoint(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        globalPosition = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
        //*/ Skip 64 /*/ FormatBytesAs.SkipBytes(Stage.reader, 8);
        unk_0x0C = FormatBytesAs.HexString(Stage.reader);
        unk_0x10 = FormatBytesAs.HexString(Stage.reader);
        localScale = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
        trackBranchID = FormatBytesAs.UInt32(Stage.reader);
    }
    public static AxCheckpoint[] LoadAxCheckpoint()
    {
        Stage.reader.BaseStream.Seek(0xAC, SeekOrigin.Begin);
        uint count  = FormatBytesAs.UInt32(Stage.reader); //0xAC
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0xB0

        AxCheckpoint[] axCheckpoint = new AxCheckpoint[count];
        for (uint i = 0; i < count; i++)
            axCheckpoint[i] = new AxCheckpoint(offset + i * size);

        return axCheckpoint;
    }

}

/// <summary>
/// 
/// Count:  0xB4
/// Offset: 0xB8
/// </summary>
[System.Serializable]
public struct StoryModeSpecialData
{
    /*/ 0x00 ?? /*/ public string leveldata;
    /*/ 0x04 96 /*/ public Vector3 triggerScale;
    /*/ 0x10 32 /*/ public uint unk_0x10; //offset?
    /*/ 0x14 96 /*/ public Vector3 scale;
    /*/ 0x20 96 /*/ public Vector3 rotation;
    /*/ 0x2C 96 /*/ public Vector3 position;
    public const uint size = 0x38;

    public StoryModeSpecialData(uint seekOffset)
    {
        Stage.reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);

        leveldata = FormatBytesAs.String(Stage.reader);
        triggerScale = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
        unk_0x10 = FormatBytesAs.UInt32(Stage.reader);
        scale    = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingScaleX);
        rotation = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingRotationX);
        position = FormatBytesAs.Vector3(Stage.reader, Stage.doInverseWindingPositionX);
    }
    public static StoryModeSpecialData[] LoadStoryModeSpecialData()
    {
        Stage.reader.BaseStream.Seek(0xB4, SeekOrigin.Begin);
        uint count = FormatBytesAs.UInt32(Stage.reader); //0xB4
        uint offset = FormatBytesAs.UInt32(Stage.reader); //0xB8

        StoryModeSpecialData[] storyData = new StoryModeSpecialData[count];
        for (uint i = 0; i < count; i++)
            storyData[i] = new StoryModeSpecialData(offset + i * size);

        return storyData;
    }

}
#endregion
