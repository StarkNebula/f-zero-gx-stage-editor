﻿//http://hitmen.c02.at/files/yagcd/yagcd/chap15.html

using UnityEngine;
using System.IO;
using System;

namespace GameCubeTexture
{
    /*
    0	I4(4 bit intensity, 8x8 tiles)
    1	I8(8 bit intensity, 8x4 tiles)
    2	IA4(4 bit intensity with 4 bit alpha, 8x4 tiles)
    3	IA8(8 bit intensity with 8 bit alpha, 4x4 tiles)
    4	RGB565(4x4 tiles)
    5	RGB5A3(*) (4x4 tiles)
    6	RGBA8(4x4 tiles in two cache lines - first is AR and second is GB)
    8	CI4(4 bit color index, 8x8 tiles)
    9	CI8(8 bit color index, 8x4 tiles)
    10	CI14X2(14 bit color index, 4x4 tiles)
    14	CMP(S3TC compressed, 2x2 blocks of 4x4 tiles)
    */
    public enum TextureFormat
    {
        I4     = 0x00,
        I8     = 0x01,
        IA4    = 0x02,
        IA8    = 0x03,
        RGB565 = 0x04,
        RGB5A3 = 0x05,
        RGBA8  = 0x06,

        CI4    = 0x08,
        CI8    = 0x09,
        CI14X2 = 0x0A, // 10

        CMPR   = 0x0E, // 14
    }

    public class BaseGameCubeImage
    {
        public Stream BaseStream;

        public virtual void SerializeAsPNG()
        {
            throw new NotImplementedException();
        }
        public virtual void DeserializeFromPNG(Stream png)
        {
            throw new NotImplementedException();
        }
    }

    public class I4 : BaseGameCubeImage
    {
        public override void SerializeAsPNG()
        {
            //BinaryReader reader = new BinaryReader(BaseStream);

            //reader.GetBytes();
        }
    }

    public struct I8
    {

    }

    public struct IA4
    {

    }

    public struct IA8
    {

    }

    public struct RGB565
    {

    }

    public struct RGB5A3
    {

    }

    public struct RGBA8
    {

    }

    public struct CI4
    {

    }

    public struct CI8
    {

    }

    public struct CMPR
    {

    }

}
