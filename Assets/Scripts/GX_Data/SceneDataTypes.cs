﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

namespace GX_Data
{
    /// <summary>
    /// GENERIC HEADER DATA FOR ALL GX DATA MEMBERS
    /// </summary>
    [Serializable] public struct GX_Header_Data
    {
        public uint address { get; private set; }
        public uint count { get; private set; }
        public uint size { get; private set; }

        // CONSTRUCTOR
        public GX_Header_Data(uint count, uint address, uint size)
        {
            this.address = address;
            this.count = count;
            this.size = size;
        }
    }
    [Serializable] public struct GX_Pointer
    {
        public uint address;// { get; private set; }
        public uint count;// { get; private set; }

        // CONSTRUCTOR
        public GX_Pointer(GX_Header_Data headerConstants, BinaryReader reader)
        {
            reader.BaseStream.Seek(headerConstants.count, SeekOrigin.Begin);
            this.count = reader.GetUInt32();

            reader.BaseStream.Seek(headerConstants.address, SeekOrigin.Begin);
            this.address = reader.GetUInt32();
        }
    }

    /// <summary>
    /// Generic data member for all GX Data types
    /// </summary>
    [Serializable] public class GX_Data
    {
        public GX_Header_Data header_const;
        public GX_Pointer header_pointer;

        public GX_Data(BinaryReader reader) { }

    }

    [Serializable] public struct GenericEntry<Type>
    {
        public uint ID_1;
        public uint ID_2;
        public Type field_1;
        public Type field_2;
        public Type field_3;

        public GenericEntry(BinaryReader reader, Func<BinaryReader, Type> BinaryReaderGetMethod)
        {
            ID_1 = reader.GetUInt32();
            ID_2 = reader.GetUInt32();

            field_1 = BinaryReaderGetMethod(reader);
            field_2 = BinaryReaderGetMethod(reader);
            field_3 = BinaryReaderGetMethod(reader);
        }
    }
    [Serializable] public struct GenericEntryFloat
    {
        public uint ID_1;
        public uint ID_2;
        public float field_1;
        public float field_2;
        public float field_3;

        public GenericEntryFloat(BinaryReader reader)
        {
            ID_1 = reader.GetUInt32();
            ID_2 = reader.GetUInt32();

            //Debug.Log("this");

            field_1 = reader.GetFloat();
            field_2 = reader.GetFloat();
            field_3 = reader.GetFloat();
        }
        new public string ToString()
        {
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", ID_1, BitConverter.ToSingle(BitConverter.GetBytes(ID_2), 0), field_1, field_2, field_3);
        }

        public Vector3 AsVector3(bool b)
        {
            return new Vector3(field_1 * ((b)?-1f:1f), field_2, field_3);
        }
    }
    [Serializable] public struct GenericEntryVector3
    {
        public uint ID_1;
        public uint ID_2;
        public Vector3 entry;

        public GenericEntryVector3(BinaryReader reader)
        {
            ID_1 = reader.GetUInt32();
            ID_2 = reader.GetUInt32();

            entry = reader.GetVector3Position();
        }
    }

    [Serializable] public class GX_Collection
    {
        public virtual void Initialize(BinaryReader reader, GX_Pointer header) { }
    }

    namespace SplineData
    {
        /// <summary>
        /// 
        /// </summary>
        [Serializable] public class SplineDataClass : GX_Data
        {
            /// HIERARCHY STRUCTURE
            /// 1. Spline Data
            ///     2. Runtime Data
            ///     2. Editor Data
            ///         3. Library Table
            ///             4. Library Entry

            // Saving space
            public List<EditorData> EditorData { get; private set; }
            public Dictionary<uint, int> EditorDataTable { get; private set; }

            public Base[] Base;

            public SplineDataClass(BinaryReader reader) : base(reader)
            {
                // Initialize GX_Data
                header_const = new GX_Header_Data(0x08, 0x0C, SplineData.Base.size);
                header_pointer = new GX_Pointer(header_const, reader);

                #region Load EditorData & EditorDataTable
                Base[] getAddresses = new Base[header_pointer.count];
                reader.BaseStream.Seek(header_pointer.address, SeekOrigin.Begin);

                //Debug.LogFormat("{0}:{1}\n{2}:{3}", header_const.address, header_instance.address, header_const.count, header_instance.count);

                for (int i = 0; i < getAddresses.Length; i++)
                    getAddresses[i] = new Base(reader);

                EditorData = new List<EditorData>();
                EditorDataTable = new Dictionary<uint, int>();
                foreach (Base c in getAddresses)
                {
                    // If the index is not yet referenced
                    if (!EditorDataTable.ContainsKey(c.EditorDataAddress))
                    {
                        // Dredge data from file, add it to list
                        EditorData.Add(new EditorData(reader, c.EditorDataAddress, null));
                        // Add this index into table
                        // Index is EditorDataTable.Count (0, 1, 2...), references the new index made above
                        EditorDataTable.Add(c.EditorDataAddress, EditorDataTable.Count);
                    }
                }
                #endregion
                #region Load Base and all sub components
                Base = new Base[header_pointer.count];
                for (uint i = 0; i < (uint)Base.Length; i++)
                {
                    Base[i] = new Base(
                        reader,
                        header_pointer.address + i * header_const.size, // i * size indicates the address for the next entry
                        EditorDataTable
                        );
                }
                #endregion
            }
        }

        /// <summary>
        /// SIZE: 3*4 (0x0C)
        /// </summary>
        [Serializable] public struct Base
        {
            public const int size = 3*4;

            /*/ 0x00 32 /*/ public uint TrackSplitType;
            /*/ 0x04 32 /*/ public uint RuntimeDataAddress;
            /*/ 0x08 32 /*/ public uint EditorDataAddress;

            //
            public RuntimeData RuntimeData { get; private set; }
            public int EditorDataID { get; private set; }

            public Base(BinaryReader reader)
            {
                //reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                // Data Members
                TrackSplitType = reader.GetUInt32();
                RuntimeDataAddress = reader.GetUInt32();
                EditorDataAddress = reader.GetUInt32();

                RuntimeData = new RuntimeData();
                EditorDataID = 0;
            }
            public Base(BinaryReader reader, uint seekAddress, Dictionary<uint, int> database)
            {
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                // Data Members
                TrackSplitType = reader.GetUInt32();
                RuntimeDataAddress = reader.GetUInt32();
                EditorDataAddress = reader.GetUInt32();

                // Lead to
                RuntimeData = new RuntimeData(reader, RuntimeDataAddress);
                EditorDataID = database[EditorDataAddress]; // Database is calculated before this constructor is called
            }
        }
        /// <summary>
        /// SIZE: 20*4 (0x50)
        /// </summary>
        [Serializable] public struct RuntimeData
        {
            public const int size = 20*4;

            /*/ 0x00 32 /*/ public float segmentStartReference;
            /*/ 0x04 32 /*/ public float segmentEndReference;
            /*/ 0x08 32 /*/ public float startingtPositionAlongSpline;
            /*/ 0x0c 96 /*/ public Vector3 startTangent;
            /*/ 0x18 96 /*/ public Vector3 startPosition;
            /*/ 0x24 96 /*/ public float endingPositionAlongSpline;
            /*/ 0x28 96 /*/ public Vector3 endTangent;
            /*/ 0x34 96 /*/ public Vector3 endPosition;
            /*/ 0x40 32 /*/ public float lengthStart;
            /*/ 0x44 32 /*/ public float lengthEnd;
            /*/ 0x48 32 /*/ public float splinePointWidth; // Looks like an editor value, since changing it has no apparent effect
            /*/ 0x4C  8 /*/ public bool mergeFromLast;
            /*/ 0x4D  8 /*/ public bool mergeToNext;
            /*/ 0x4E 16 /*/ //NULL

            public RuntimeData(BinaryReader reader, uint seekAddress)
            {
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                segmentStartReference = reader.GetFloat();
                segmentEndReference = reader.GetFloat();
                startingtPositionAlongSpline = reader.GetFloat();
                startTangent = reader.GetVector3Normal();
                startPosition = reader.GetVector3Position();
                endingPositionAlongSpline = reader.GetFloat();
                endTangent = reader.GetVector3Normal();
                endPosition = reader.GetVector3Position();
                lengthStart = reader.GetFloat();
                lengthEnd = reader.GetFloat();
                splinePointWidth = reader.GetFloat();
                mergeFromLast = reader.GetBool();
                mergeToNext = reader.GetBool();
                //reader.SkipBytes(2); // Not needed as constructor call will reset seek position
            }
        }
        /// <summary>
        /// SIZE: 20*4 (0x50)
        /// </summary>
        [Serializable] public class EditorData
        {
            public const int size = 20*4;

            /*/ 0x00 16 /*/ public byte hierarchyPositionFlag; // 8-bit parent layer
            /*/ 0x02 16 /*/ public byte hasSubEditorData; //Children == [0C00]
            /*/ 0x04 32 /*/ public uint libraryAddress;
            /*/ 0x08 32 /*/ //NULL
            /*/ 0x0C 32 /*/ public uint childEditorDataCount;
            /*/ 0x10 32 /*/ public uint childEditorDataAddress; // Has something to do with spline branches
            /*/ 0x14 96 /*/ public Vector3 localScale;
            /*/ 0x20 96 /*/ public Quaternion localRotation;
            /*/ 0x2C 96 /*/ public Vector3 localPosition;
            /*/ 0x38 32 /*/ public uint unk_id; // Power of Two ID
            /*/ 0x3C 32 /*/ public float unk0x3C; // 0x3C and 0x40 are typically the same
            /*/ 0x40 32 /*/ public float unk0x40;
            /*/ 0x44 96 /*/ //NULL (VECTOR?)

            // INTERNAL STRUCTS
            public LibraryTable LibraryIndexTable { get; private set; }
            public EditorData[] childEditorData;
            public EditorData parent;
            public Vector3 GlobalPosition = new Vector3();
            public Quaternion GlobalRotation = new Quaternion();

            // CONSTRUCTOR
            public EditorData(BinaryReader reader, uint seekAddress, EditorData parent)
            {
                this.parent = parent;
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                hierarchyPositionFlag = reader.GetByte(); // 8-bit parent layer
                /*/ NULL 08 /*/ reader.SkipBytes(1);
                hasSubEditorData = reader.GetByte(); // 0x0C indicates children exist
                /*/ NULL 08 /*/ reader.SkipBytes(1);
                libraryAddress = reader.GetUInt32(); // NEW EDITORDATA
                /*/ NULL 32 /*/ reader.SkipBytes(4);
                childEditorDataCount = reader.GetUInt32();
                childEditorDataAddress = reader.GetUInt32();
                localScale = reader.GetVector3Scale();
                localRotation = reader.GetQuaternionRotation();
                localPosition = reader.GetVector3Position();
                unk_id = reader.GetUInt32();
                unk0x3C = reader.GetFloat();
                unk0x40 = reader.GetFloat();
                /*/ NULL 96 /*/ reader.SkipBytes(12);

                LibraryIndexTable = new LibraryTable(reader, libraryAddress);

                if (parent != null)
                {
                    GlobalPosition += parent.GlobalPosition; // not doing this right, do rotation AFTER
                    GlobalRotation *= parent.GlobalRotation;
                }
                GlobalPosition += localPosition;
                GlobalRotation *= localRotation;

                // Recursive
                childEditorData = new EditorData[childEditorDataCount];
                for (uint ui = 0; ui < childEditorDataCount; ui++)
                    childEditorData[ui] = new EditorData(reader, childEditorDataAddress + ui * size, this);


            }


        }
        /// <summary>
        /// SIZE: 18*4 (0x48)
        /// </summary>
        [Serializable] public struct LibraryTable
        {
            /*/ 0x00 32 * 9 /*/ public uint[] count; // Int temp. to fix issues. Not all paths return uint
            /*/ 0x24 32 * 9 /*/ public uint[] address; // Can be 0x0, so you have to only jump when not 0x0!

            // INTERNAL STRUCTS
            public GenericEntryFloat[][] LibraryEntry { get; private set; } // 9x?


            public LibraryTable(BinaryReader reader, uint seekAddress)
            {
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                count   = new uint[9];
                address = new uint[9];
                for (int i = 0; i < 9; i++) count[i]   = reader.GetUInt32();
                for (int i = 0; i < 9; i++) address[i] = reader.GetUInt32();

                #region Initialize LibraryEntry
                // Set constant size to 9
                LibraryEntry = new GenericEntryFloat[9][];

                // Method to read data for LibraryEntry
                Func<BinaryReader, float> BinaryReader_GetFloat = delegate (BinaryReader r) { return r.GetFloat(); };

                for (int i = 0; i < 9; i++)
                {
                    // Set the array size of each Entry to the count in the LibraryTable
                    LibraryEntry[i] = new GenericEntryFloat[count[i]];

                    if (count[i] > 0)
                    {
                        // Go to address
                        reader.BaseStream.Seek(address[i], SeekOrigin.Begin);

                        // Read all entries
                        for (int j = 0; j < count[i]; j++)
                            LibraryEntry[i][j] = new GenericEntryFloat(reader);
                    }
                }
                #endregion
            }
        }
    }
    namespace Other
    {
        [Serializable]
        public class GxDataOther : GX_Data
        {
            public FzgxObject[] obj;

            public GxDataOther(BinaryReader reader) : base(reader)
            {
                header_const = new GX_Header_Data(0x48, 0x54, 16 * 4);
                header_pointer = new GX_Pointer(header_const, reader);

                obj = new FzgxObject[header_pointer.count];
                for (uint ui = 0; ui < obj.Length; ui++)
                {
                    obj[ui] = new FzgxObject(reader, header_pointer.address + ui * FzgxObject.size);
                }
            }
        }
        [Serializable]
        // Non Interactive Object
        public struct FzgxObject
        {
            public const uint size = 0x40;

            // IIRC, this is an object prefab
            // Using the last index we get to all the instances of it in the scene
            //
            /*/ 0x00 32 /*/ public uint id1; // ID? - Varies A LOT, but seems to be 4 IDs
            /*/ 0x04 32 /*/ public uint id2; // FFFF FFFF or 8000 0000?
            /*/ 0x08 32 /*/ public uint lodAndCollisionAddress; // Offset to the collision for this object - part of 0x64 header
            /*/ 0x0C 32 /*/ public Vector3 position; // Confirmed position x y z
            /*/ 0x18 32 /*/ public string unknown0x18; // ?
            /*/ 0x1C 32 /*/ public string unknown0x1C; // ID?
            /*/ 0x20 32 /*/ public Vector3 scale; // Confirmed scale x y z
            /*/ 0x2C 32 /*/ //NULL 32
            /*/ 0x30 32 /*/ public uint animationAddress;   // Offset 2
            /*/ 0x34 32 /*/ public uint unknownAddress0x34; // Offset 3
            /*/ 0x38 32 /*/ public uint skeletonAdress;     // Offset 4 - SKL models
            /*/ 0x3C 32 /*/ public uint transformAddress;   // Transform: scale, rotation, position


            // Offset 2 - Animation
            public ObjectAnimation animation;
            // Offset 5 - Transform
            public ObjectTransform transform;

            public FzgxObject(BinaryReader reader, uint seekAddress)
            {
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                id1 = reader.GetUInt32();
                id2 = reader.GetUInt32();
                lodAndCollisionAddress = reader.GetUInt32();
                position = reader.GetVector3Position();
                unknown0x18 = reader.GetString(4);
                unknown0x1C = reader.GetString(4);
                scale = reader.GetVector3Scale();
                /*/ NULL 32 /*/
                reader.SkipBytes(4);
                animationAddress = reader.GetUInt32();
                unknownAddress0x34 = reader.GetUInt32();
                skeletonAdress = reader.GetUInt32();
                transformAddress = reader.GetUInt32();

                //
                transform = new ObjectTransform(reader, transformAddress);
                animation = new ObjectAnimation(reader, animationAddress);
            }
        }
        [Serializable]
        public struct ObjectAnimation
        {
            public float dat_0x00;
            public float dat_0x04;
            public uint layer;
            public GX_Pointer[] pointers;

            //
            public AnimationEntry[][] animationEntry;
            // 1 -
            // 2 -
            // 3 -
            // 4 - Position
            // 5 - Rotation
            // 6 - Rotation

            public ObjectAnimation(BinaryReader reader, uint seekAddress)
            {
                reader.BaseStream.Seek(seekAddress, SeekOrigin.Begin);

                dat_0x00 = reader.GetFloat();
                dat_0x04 = reader.GetFloat();
                reader.SkipBytes(4 * 4);

                layer = reader.GetUInt32();
                //reader.SkipBytes(2);

                pointers = new GX_Pointer[6];
                animationEntry = new AnimationEntry[6][];

                for (int i = 0; i < 6; i++)
                {
                    reader.SkipBytes(4 * 4);
                    pointers[i].count = reader.GetUInt32();
                    pointers[i].address = reader.GetUInt32();
                    animationEntry[i] = new AnimationEntry[pointers[i].count];
                }

                for (int i = 0; i < 6; i++)
                {
                    reader.BaseStream.Seek(pointers[i].address, SeekOrigin.Begin);
                    for (int j = 0; j < animationEntry[i].Length; j++)
                        animationEntry[i][j] = new AnimationEntry(reader);
                }
            }
        }

        public struct AnimationEntry
        {
            public uint id;
            public float time;
            public Vector3 vector;

            public AnimationEntry(BinaryReader reader)
            {
                id = reader.GetUInt32();
                time = reader.GetFloat();
                vector = reader.GetVector3Position();
            }
        }


        [Serializable]
        public struct ObjectTransform
        {
            public Vector3 normalX;
            public Vector3 normalY;
            public Vector3 normalZ;
            public Vector3 position;

            public Quaternion rotation;
            public Vector3 scale;


            public ObjectTransform(BinaryReader reader, uint seekPosition)
            {
                reader.BaseStream.Seek(seekPosition, SeekOrigin.Begin);

                Vector3 position = new Vector3();

                normalX = reader.GetVector3Normal();
                position.x = reader.GetFloat() * ((Stage.doInverseWindingPositionX) ? -1f : 1f);
                normalY = reader.GetVector3Normal();
                position.y = reader.GetFloat();
                normalZ = reader.GetVector3Normal();
                position.z = reader.GetFloat();

                this.position = position;
                this.scale = new Vector3(normalX.magnitude, normalY.magnitude, normalZ.magnitude);
                rotation = Quaternion.Euler(Vector3.Angle(normalX, Vector3.right), Vector3.Angle(normalY, Vector3.up), Vector3.Angle(normalZ, Vector3.forward));
            }
        }
        


        public class AiEffects : GX_Data
        {
            public AiEffects(BinaryReader reader) : base(reader)
            {
                header_const = new GX_Header_Data(0x10, 0x14, 5u * 4);
                header_pointer = new GX_Pointer(header_const, reader);
            }
        }
    }
}