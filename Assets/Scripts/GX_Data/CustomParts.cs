﻿using UnityEngine;
using System.Collections;

public class CustomParts : MonoBehaviour
{
    public enum Body
    {
        Aqua_Goose,
        Big_Tyrant,
        Brave_Eagle,
        Dread_Hammer,
        Fire_Wolf,
        Funny_Swallow,
        Giant_Planet,
        Grand_Base,
        Holy_Spider,
        Liberty_Manta,
        Megalo_Cruiser,
        Optical_Wing,
        Rapid_Barrel,
        Silver_Sword,
        Sky_Horse,
        Space_Cancer,
        Speedy_Dragon,
        Splash_Whale,
        Valiant_Jaguar,
        Wild_Chariot,
        Mad_Bull,      //AX
        Blood_Raven,   //JP
        Galaxy_Falcon, //JP
        Metal_Shell,   //JP
        Rage_Knight,   //JP
    }
    public enum Cockpit
    {
        Aerial_Bullet,
        Blast_Camel,
        Bright_Spear,
        Combat_Cannon,
        Crystal_Egg,
        Cyber_Fox,
        Dark_Chaser,
        Energy_Crest,
        Garnet_Phantom,
        Heat_Snake,
        Moon_Snail,
        Muscle_Gorilla,
        Rave_Drifter,
        Red_Rex,
        Scud_Viper,
        Sonic_Soldier,
        Spark_Bird,
        Super_Lynx,
        Windy_Shark,
        Wonder_Worm,
        Crazy_Buffalo, //AX
        Hyper_Stream,  //JP
        Maximum_Star,  //JP
        Round_Disk,    //JP
        Rush_Cyclone   //JP
    }
    public enum Booster
    {
        // '_' == " -"
        Bazooka_YS,
        Bluster_X,
        Boxer_2C,
        Comet_V,
        Devilfish_RX,
        Euros_01,
        Extreme_ZZ,
        Impulse220, //Space only
        Jupiter_Q,
        Meteor_RR,
        Punisher_4X,
        Raiden_88,
        Saturn_SG,
        Scorpion_R,
        Sunrise140, //Space only
        Thunderbolt_V2,
        Tiger_RZ,
        Titan_G4,
        Triangle_GT,
        Triple_Z,
        Mars_EX,    //AX
        Crown_77,   //JP
        Hornet_FX,  //JP
        Shuttle_M2, //JP
        Velocity_J  //JP
    }

    public Body body = 0;
    public Cockpit cockpit = 0;
    public Booster booster = 0;
}