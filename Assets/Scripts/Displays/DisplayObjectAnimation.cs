﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using GX_Data;
using GX_Data.Other;
using GX_Data.SplineData;

public class DisplayObjectAnimation : MonoBehaviour
{
    static GxDataOther gxDataOther;

    #region CALLBACK LOADING
    [UnityEditor.Callbacks.DidReloadScripts]
    static void Reset()
    {
        Stage.StageChangedCallback += delegate { StageChangedEvent(); };
    }
    static void StageChangedEvent()
    {
        gxDataOther = new GxDataOther(Stage.reader);
        PrintOut();
    }
    #endregion

    void OnDrawGizmos()
    {

        foreach (FzgxObject obj in gxDataOther.obj)
        {
            for (int i = 0; i < obj.animation.animationEntry.Length; i++)
            {
                Gizmos.color = Palette.ColorWheel(i, 6).SetAlpha(.5f);
                int length = obj.animation.animationEntry[i].Length;

                for (int j = 0; j < obj.animation.animationEntry[i].Length; j++)
                {
                    int @case = j / 2;

                    if (@case == 0)
                    {

                    }
                    else if (@case == 1)
                    {
                        Gizmos.DrawLine(
                            obj.animation.animationEntry[i][j].vector + obj.transform.position,
                            obj.animation.animationEntry[i][MathX.Wrap(j + 1, length)].vector + obj.transform.position
                            );
                        Gizmos.DrawCube(obj.animation.animationEntry[i][j].vector, Vector3.one * 10f);
                    }
                    else if (@case == 2)
                    {

                    }
                }
            }
        }
    }



    public static void PrintOut()
    {
        // Print out animations
        //*/
        for (int h = 0; h < 6; h++)
        {
            for (int i = 0; i < gxDataOther.obj.Length; i++) // 420
            {
                for (int j = 0; j < gxDataOther.obj[i].animation.animationEntry.Length; j++) // 6
                {
                    for (int k = 0; k < gxDataOther.obj[i].animation.animationEntry[j].Length; k++) // ??
                    {
                        if (h == j)
                        {
                            if (gxDataOther.obj[i].animation.animationEntry[j].Length > 0)
                            {
                                PrintLog.WriteTsvLineToBuffer(
                                    i.ToString(),
                                    j.ToString(),
                                    k.ToString(),
                                    gxDataOther.obj[i].animation.animationEntry[j][k].id.ToString(),
                                    gxDataOther.obj[i].animation.animationEntry[j][k].time.ToString(),
                                    gxDataOther.obj[i].animation.animationEntry[j][k].vector.x.ToString(),
                                    gxDataOther.obj[i].animation.animationEntry[j][k].vector.y.ToString(),
                                    gxDataOther.obj[i].animation.animationEntry[j][k].vector.z.ToString()
                                    );
                            }
                        }
                    }
                }
            }
            PrintLog.SaveStream(Stage.currentStage.ToString() + (h + 1).ToString());
        }
        //*/
    }
}