﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;

using GX_Data;
using GX_Data.Other;
using GX_Data.SplineData;

public class DisplayObjectTransform : MonoBehaviour
{
    // Draw Simple Variables
    public bool drawSimple = true;
    public float drawSimpleScale = 5f;
    public float drawSimpleScaleMax = 20f;
    public Color drawSimpleColor = Palette.white.SetAlpha(.5f);
    public Mesh drawSimpleMesh;

    static GxDataOther otherData;
    #region CALLBACK LOADING
    [UnityEditor.Callbacks.DidReloadScripts]
    static void Reset()
    {
        Stage.StageChangedCallback += delegate { StageChangedEvent(); };
    }
    static void StageChangedEvent()
    {
        otherData = new GxDataOther(Stage.reader);
    }
    #endregion

    void OnDrawGizmos()
    {
        if (otherData == null) return;

        if (drawSimple)
            DrawSimple(otherData);
        else
            DrawComplexeTransform(otherData);
    }

    public void DrawSimple(GxDataOther gxData)
    {
        Gizmos.color = drawSimpleColor;

        foreach (FzgxObject obj in gxData.obj)
        {
            //Gizmos.color = ColorUtility.NormalToColor(obj.transform.rotation * Vector3.forward);
            Gizmos.DrawMesh(drawSimpleMesh, obj.transform.position, obj.transform.rotation, Vector3Utility.Clamp(obj.transform.scale * drawSimpleScale, -drawSimpleScaleMax, drawSimpleScaleMax));
        }

        Gizmos.color = Palette.red_orange;
        foreach (FzgxObject obj in gxData.obj)
            GizmosDrawTransform(obj, Vector3.right, obj.transform.scale.x);

        Gizmos.color = Palette.lime_green;
        foreach (FzgxObject obj in gxData.obj)
            GizmosDrawTransform(obj, Vector3.up, obj.transform.scale.y);

        Gizmos.color = Palette.cobalt;
        foreach (FzgxObject obj in gxData.obj)
            GizmosDrawTransform(obj, Vector3.forward, obj.transform.scale.z);
    }
    public void GizmosDrawTransform(FzgxObject obj, Vector3 direction, float directionScale)
    {
        Gizmos.DrawLine(
            obj.transform.position,
            obj.transform.position + obj.transform.rotation * direction * Mathf.Clamp(directionScale * drawSimpleScale, -drawSimpleScaleMax, drawSimpleScaleMax)
            );
    }

    public void DrawComplexeTransform(GxDataOther gxData)
    {
        // X
        Handles.color = Palette.red_orange;
        foreach (FzgxObject g in gxData.obj)
        {
            Handles.DrawWireDisc(g.transform.position, g.transform.normalX, 5f);
            Handles.DrawLine(g.transform.position, g.transform.position + g.transform.normalX * 5F);
            //SmartHandleLabel(g.Transform.position, Vector3.zero, "X: " + g.Transform.normalX.magnitude.ToString());
        }

        // Y
        Handles.color = Palette.lime;
        foreach (FzgxObject g in gxData.obj)
        {
            Handles.DrawWireDisc(g.transform.position, g.transform.normalY, 5f);
            Handles.DrawLine(g.transform.position, g.transform.position + g.transform.normalY * 5F);
            //SmartHandleLabel(g.Transform.position, Vector3.down * .2f, "Y: " + g.Transform.normalY.magnitude.ToString());
        }

        // Z
        Handles.color = Palette.cobalt;
        foreach (FzgxObject g in gxData.obj)
        {
            Handles.DrawWireDisc(g.transform.position, g.transform.normalZ, 5f);
            Handles.DrawLine(g.transform.position, g.transform.position + g.transform.normalZ * 5F);
            //SmartHandleLabel(g.Transform.position, Vector3.down*.4f, "Z: " + g.Transform.normalZ.magnitude.ToString());

            SmartHandleLabel(g.transform.position, Vector3.down * .2f, "Position: " + g.transform.position.ToString());
            SmartHandleLabel(g.transform.position, Vector3.down * .4f, "Rotation: " + g.transform.rotation.eulerAngles.ToString());
            SmartHandleLabel(g.transform.position, Vector3.down * .6f, "Scale:    " + g.transform.scale.ToString());
        }
    }
    public void SmartHandleLabel(Vector3 position, Vector3 offset, string label)
    {
        if (Vector3.Dot(Camera.current.transform.forward, position - Camera.current.transform.position) > 0)
            if (Vector3.Distance(Camera.current.transform.position, position) < 300f)
                Handles.Label(position + offset * HandleUtility.GetHandleSize(position + offset), label);
    }

}