﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

using GX_Data;
using GX_Data.SplineData;

public class DisplaySpline : MonoBehaviour
{
    static SplineDataClass SplineData;

    [UnityEditor.Callbacks.DidReloadScripts]
    static void Reset()
    {
        Stage.StageChangedCallback += delegate { StageChangedEvent(); };
    }
    static void StageChangedEvent()
    {
        SplineData = new SplineDataClass(Stage.reader);
    }

    void OnDrawGizmos()
    {
        if (SplineData == null) return;
        ///////////////////////////////

        for (int i = 0; i < SplineData.Base.Length; i++)
        {
            Gizmos.color = Handles.color = Palette.ColorWheel(SplineData.Base[i].EditorDataID, SplineData.EditorData.Count);
            Gizmos.color = (SplineData.Base[i].EditorDataID % 2 == 0) ? Gizmos.color : Gizmos.color.Whitten(.75f);
            Handles.DrawDottedLine(SplineData.Base[i].RuntimeData.startPosition, SplineData.Base[i].RuntimeData.endPosition, 2f);

            Handles.DrawWireDisc(SplineData.Base[i].RuntimeData.startPosition, SplineData.Base[i].RuntimeData.startTangent, 2.5f);
            Handles.DrawWireDisc(SplineData.Base[i].RuntimeData.endPosition,   SplineData.Base[i].RuntimeData.endTangent, 5.0f);
        }
    }
}