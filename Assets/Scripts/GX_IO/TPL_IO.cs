﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
//using System.Drawing;

public static class TPL_IO
{
    public static void ParseTPL(Stream stream)
    {
        BinaryReader reader = new BinaryReader(stream);
        uint entryCount = reader.GetUInt32();

        List<TPL_Entry> entries = new List<TPL_Entry>();

        // For each entry in TPL
        for (int i = 0; i < entryCount; i++)
        {
            // If it is not the end of the TPL header
            if (reader.GetUInt16() != 1)
            {
                // If an image is present in this slot
                if (reader.GetBool())
                {
                    // Store current position as new TPL_Entry will seek new offset
                    long pos = reader.BaseStream.Position;

                    // Add new entry to TPL list
                    entries.Add(new TPL_Entry(reader, i));

                    // Reset offset to next entry, which is last entry + 13 bytes (we read 3/16 already).
                    reader.BaseStream.Seek(pos + 13, SeekOrigin.Begin);
                }
                else
                {
                    // ENTRY IS NULL
                    reader.BaseStream.Seek(reader.BaseStream.Position + 13, SeekOrigin.Begin);
                }
            } 
        } // for loop
    }

}


public struct TPL_Entry
{
    public int id;

    public GameCubeTexture.TextureFormat textureFormat;
    public uint offset;
    public ushort width, height;
    public ushort subdivideCount;
    public Stream imageData;

    public TPL_Entry(BinaryReader reader, int i)
    { 
        id = i;

        textureFormat  = (GameCubeTexture.TextureFormat)reader.GetByte();
        offset         = reader.GetUInt32();
        width          = reader.GetUInt16();
        height         = reader.GetUInt16();
        subdivideCount = reader.GetUInt16();

        // SOLVE LENGTH
        reader.BaseStream.Seek(offset, SeekOrigin.Begin);
        imageData = new MemoryStream(reader.ReadBytes(11));
    }

    public void SaveAS()
    {
        //Image i = Image.FromFile();
    }
}