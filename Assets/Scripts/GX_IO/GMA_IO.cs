﻿using UnityEngine;
using System.Collections;

public class GMA_IO
{

}

public struct GMA_Entry
{
    public string name;

    // GCMF
    /*/ 0x04 32 /*/ public bool isSpecialCaseModel;
    /*/ 0x08 96 /*/ public Vector3 origin;
    /*/ 0x14 32 /*/ public float radius;
    /*/ 0x18 16 /*/ public ushort textureCount;
    /*/ 0x1A 16 /*/ public ushort materialCount;
    /*/ 0x1C 16 /*/ public ushort materialTLCount;
    /*/ 0x1E 16 /*/ //null
    /*/ 0x20 32 /*/ public uint vertexDataOffset;
    /*/ 0x24 32 /*/ //null
    /*/ 0x28 64 /*/ // FFFF FFFF FFFF FFFF

    public GMA_Texture_Entry[] library;

    /*/ 0x00 32 /*/ public uint lightShader; //?
    /*/ 0x04 32 /*/ public Color32 colorA;
    /*/ 0x08 32 /*/ public Color32 colorB;
    /*/ 0x0C 16 /*/ public ushort unk_0C;
    /*/ 0x0E 16 /*/ public ushort unk_0E;
    /*/ 0x10 08 /*/ public byte unk_10; // is this with FF make a short?
    /*/ 0x11 08 /*/ public const byte unk_11 = 0xFF;
    /*/ 0x12 08 /*/ public byte flagFor_14;
    /*/ 0x13 08 /*/ public byte textureType;
    /*/ 0x14 08 /*/ public byte unkFlag;
    /*/ 0x15 24 /*/ //null
    /*/ 0x18 32 /*/ // FFFF FFFF
    /*/ 0x1C 32 /*/ public int modelCode;
    /*/ 0x20 64 /*/ // FFFF FFFF FFFF FFFF
    /*/ 0x28 32 /*/ public int tex1Length;
    /*/ 0x2C 32 /*/ public int tex2Length;
    /*/ 0x30 96 /*/ public Vector3 unk_30; // UVW map?
    /*/ 0x3C 32 /*/ //null
    /*/ 0x40 32 /*/ public int unk_40;
    /*/ 0x44 224 /*/ //null - 28 bytes, 7 ints
}

public struct GMA_Texture_Entry
{
    /*/ 0x00 16 /*/ public ushort textureFlag;
    /*/ 0x02 08 /*/ public byte modellingType;
    /*/ 0x03 08 /*/ public byte wrapMode; //bitmask
    /*/ 0x04 16 /*/ public ushort tplIndex;
    /*/ 0x06 08 /*/ public byte preAniso;
    /*/ 0x07 08 /*/ public byte aniso;
    /*/ 0x08 32 /*/ private const ulong const_08 = 0xFFFFFFFFFFFFFFFF;
    /*/ 0x0C 08 /*/ public byte unk;
    /*/ 0x0D 08 /*/ //null
    /*/ 0x0E 16 /*/ public ushort textureNumber;
    /*/ 0x10 32 /*/ private const uint const_10 = 30;
    /*/ 0x14 96 /*/ //null
}


/// <summary>
/// GCMF WrapMode for textures. TO BE SAVED AS BYTE.
/// </summary>
public enum WrapMode
{
    Primitive_A_Clamp_Clamp   = 0x00, // 00 00 00 00
    Primitive_A_Clamp_Repeat  = 0x10, // 00 01 00 00
    Primitive_A_Clamp_Mirror  = 0x20, // 00 10 00 00
    Primitive_A_Repeat_Clamp  = 0x04, // 00 00 01 00
    Primitive_A_Repeat_Repeat = 0x14, // 00 01 01 00
    Primitive_A_Repeat_Mirror = 0x24, // 00 10 01 00
    Primitive_A_Mirror_Clamp  = 0x08, // 00 00 10 00
    Primitive_A_Mirror_Repeat = 0x18, // 00 01 10 00
    Primitive_A_Mirror_Mirror = 0x28, // 00 10 10 00

    Primitive_B_Clamp_Clamp   = 0x40, // 01 00 00 00
    Primitive_B_Clamp_Repeat  = 0x50, // 01 01 00 00
    Primitive_B_Clamp_Mirror  = 0x60, // 01 10 00 00
    Primitive_B_Repeat_Clamp  = 0x44, // 01 00 01 00
    Primitive_B_Repeat_Repeat = 0x54, // 01 01 01 00
    Primitive_B_Repeat_Mirror = 0x64, // 01 10 01 00
    Primitive_B_Mirror_Clamp  = 0x48, // 01 00 10 00
    Primitive_B_Mirror_Repeat = 0x58, // 01 01 10 00
    Primitive_B_Mirror_Mirror = 0x68, // 01 10 10 00

    Standard_A_Clamp_Clamp    = 0x80, // 10 00 00 00
    Standard_A_Clamp_Repeat   = 0x90, // 10 01 00 00
    Standard_A_Clamp_Mirror   = 0xA0, // 10 10 00 00
    Standard_A_Repeat_Clamp   = 0x84, // 10 00 01 00
    Standard_A_Repeat_Repeat  = 0x94, // 10 01 01 00
    Standard_A_Repeat_Mirror  = 0xA4, // 10 10 01 00
    Standard_A_Mirror_Clamp   = 0x88, // 10 00 10 00
    Standard_A_Mirror_Repeat  = 0x98, // 10 01 10 00
    Standard_A_Mirror_Mirror  = 0xA8, // 10 10 10 00

    Standard_B_Clamp_Clamp    = 0xC0, // 11 00 00 00
    Standard_B_Clamp_Repeat   = 0xD0, // 11 01 00 00
    Standard_B_Clamp_Mirror   = 0xE0, // 11 10 00 00
    Standard_B_Repeat_Clamp   = 0xC4, // 11 00 01 00
    Standard_B_Repeat_Repeat  = 0xD4, // 11 01 01 00
    Standard_B_Repeat_Mirror  = 0xE4, // 11 10 01 00
    Standard_B_Mirror_Clamp   = 0xC8, // 11 00 10 00
    Standard_B_Mirror_Repeat  = 0xD8, // 11 01 10 00
    Standard_B_Mirror_Mirror  = 0xE8, // 11 10 10 00
}
/// <summary>
/// Anisotropic filter.
/// </summary>
public enum ANISO
{
    None = 0,
    ANISO_2 = 1,
    ANISO_4 = 2,
}
